# Changelog

  - 2018-02-21: Bailey Parker
    - Refactored out `MovingObject` (`Player` now solely relies on its
      `Rigidbody2d`). This fixes some bugs and makes the code a lot
      simpler.
    - Refactored `CameraControll` to not be so tightly coupled with the player
    - Refactored `LevelManager`
    - Made better use of `UnityEngine.Assertions`
    - Identified bug in `Projectile`, removed its dependency on the `Player`
      for now
