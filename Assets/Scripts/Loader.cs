﻿using UnityEngine;


public class Loader : MonoBehaviour {
    public GameObject gameManager;
    public GameObject soundManager;

    void Awake() {
        if (GameManager.Instance == null) {
            Instantiate(this.gameManager);
        }

        if (SoundManager.Instance == null) {
            Instantiate(soundManager);
        }
    }
}
