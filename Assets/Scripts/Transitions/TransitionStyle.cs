using UnityEngine;


public enum TransitionStyle {
    // NOTE: Do not change these numbers!
    None = 0,
    BindingSwipe = 1,
    ZoomInThenOut = 2,
    BatmanSpin = 3,
    FadeToBlack = 4,
    BlackFrames = 5,
}

public static class TransitionStyleExtensions {
    public static Transition Instantiate(this TransitionStyle style, float duration,
                                         GameObject origin, GameObject destination,
                                         string entryDoor, GameObject transitionObject) {
        switch (style) {
            case TransitionStyle.None:
                return new NoTransition(origin, destination, entryDoor,
                                        transitionObject);
            case TransitionStyle.BindingSwipe:
                return new BindingSwipeTransition(duration, origin, destination,
                                                  entryDoor, transitionObject);
            case TransitionStyle.ZoomInThenOut:
                return new ZoomInThenOutTransition(duration, origin, destination,
                                                   entryDoor, transitionObject);
            case TransitionStyle.BatmanSpin:
                // TODO(bp): configurable spins
                return new BatmanSpinTransition(duration, origin, destination,
                                                entryDoor, transitionObject, 15);
            case TransitionStyle.FadeToBlack:
                return new FadeToBlackTransition(duration, origin, destination,
                                                 entryDoor, transitionObject);
            case TransitionStyle.BlackFrames:
                return new BlackFramesTransition(duration, origin, destination,
                                                 entryDoor, transitionObject);
            default:
                Debug.LogError(string.Format("Illegal enum value {0}", style));
                return null;
        }
    }
}
