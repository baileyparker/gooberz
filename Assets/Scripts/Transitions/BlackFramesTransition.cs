using UnityEngine;


public class BlackFramesTransition : BlackoutTransition {
    public BlackFramesTransition(float duration, GameObject origin,
                                 GameObject destination, string entryDoor,
                                 GameObject transitionObject)
        : base(duration, origin, destination, entryDoor, transitionObject) {}

    protected override void Update(float percentComplete) {
        Blackout.SetActive(true);
    }
}
