using UnityEngine;


public class BlackoutTransition : Transition {
    protected GameObject Blackout {
        // TODO(bp): perhaps use tags, this is fragile
        get { return TransitionObject.transform.GetChild(0).gameObject; }
    }


    public BlackoutTransition(float duration, GameObject origin,
                              GameObject destination, string entryDoor,
                              GameObject transitionObject)
        : base(duration, origin, destination, entryDoor, transitionObject) {}

    protected override void Finish() {
        Blackout.SetActive(false);
        base.Finish();
    }
}
