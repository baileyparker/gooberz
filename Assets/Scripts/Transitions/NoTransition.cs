using UnityEngine;


public class NoTransition : Transition {
    public NoTransition(GameObject origin, GameObject destination, string entryDoor,
                        GameObject transitionObject)
        : base(0, origin, destination, entryDoor, transitionObject) {}
}
