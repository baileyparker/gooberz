﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ElevatorLevelTransition : MonoBehaviour {

    public enum ElevatorActionType {
        EnterOnly,
        ExitOnly,
        EnterAndExit,
    }

    [Header("Elevator Behavior")]
    public ElevatorActionType ElevatorAction = ElevatorActionType.EnterAndExit;
    public RoomName ElevatorToRoom; //not required if ExitOnly elevator
    public SoundEnum MusicInThisRoom = SoundEnum.none;
    public string JanitorDialogOnEntry = "";
    public string JanitorDialogOnExit = "";
    public float elevatorSpeed = 10f; //unity units per second
    public float maxShakeOffset = 0.025f;
    public float maxShakeAngle = 3f;


    [Header("Game Objects")]
    public GameObject LeftDoor;
    public GameObject RightDoor;
    public GameObject WallBehindLeftDoor; //walls behind elevator need to be disabled
    public GameObject WallBehindRightDoor;
    public GameObject batteryPrefab;

    private GameObject batteryObj;
    private GameObject player;

    bool entryInProgress = false;
    bool exitInProgress = false;
    bool firstFrame = true;
    bool talkedYet = false;

    float startTime;
    Vector3 LeftDoorInitPos;
    Vector3 RightDoorInitPos;
    Vector3 playerStartPos;
    Vector3 camStartPos;
    Vector3 playerPlaceBatteryPos;// = new Vector3(-9.25f, 7.4f, 0);
    Vector3 batteryPlacePos;// = new Vector3(-9.25f, 8, 0);
    Vector3 playerFrontOfElevatorPos;// = new Vector3(-7.5f, 6.25f, 0);
    Vector3 camFrontofElevatorPos;// = new Vector3(-7.5f, 6.5f, -10f);
    Vector3 playerInsideElevatorPos;// = new Vector3(-7.5f, 8.5f, 0);
    Vector3 camPanUpPos;// = new Vector3(-7.5f, 16f, -10f);


    //start at t=0 when the player collides with the narrative trigger
    //for each of these, force the player to be in the same spot (maybe deactivate player control script)
    float key0 = 2.0f;  // walk to place battery (set animation to walking)
    float key1 = 3.0f;  // place battery
    float key2 = 5.0f;  // walk back
    float key3 = 7.0f;  // elevator does stuff (shake)
    float key4 = 9.0f;  // elevator doors open
    float key5 = 11.0f; // player walks in (set elevator door layer to in front of player)
    float key6 = 13.0f;  // doors close
    float key7;// = 14.0f;  // camera pans up (i.e. elevator is moving up)





    //start at t=0 when the player collides with the narrative trigger
    //for each of these, force the player to be in the same spot (maybe deactivate player control script)

    //key 8 is set at runtime, and then added to all the other keys
    float key8;// = 2f;  //finish panning the camera up to the elevator
    float key9 = 0.5f;  //pause for a moment
    float key10 = 2.5f; //open the doors
    float key11 = 4.5f; //player walks out
    float key12 = 6.5f; //elevator doors close






    // Use this for initialization
    void Start () {
        player = GameObject.Find("Player");

        LeftDoorInitPos = LeftDoor.transform.position;
        RightDoorInitPos = RightDoor.transform.position;

        //set up vectors to move objects, based off of the position of the left door
        Vector3 origin = LeftDoorInitPos;

        playerPlaceBatteryPos = origin + Vector3.left * 1.35f + Vector3.down * 1.4f; // = new Vector3(-9.25f, 7.4f, 0);
        batteryPlacePos = origin + Vector3.left * 1.35f + Vector3.down * 0.8f; //= new Vector3(-9.25f, 8, 0);
        playerFrontOfElevatorPos = origin + Vector3.right * 0.4f + Vector3.down * 2.55f; // = new Vector3(-7.5f, 6.25f, 0);
        camFrontofElevatorPos = new Vector3(origin.x + 0.4f, origin.y - 2.3f, -10); // = new Vector3(-7.5f, 6.5f, -10f);
        playerInsideElevatorPos = origin + Vector3.right * 0.4f + Vector3.down * 0.3f; // = new Vector3(-7.5f, 8.5f, 0);
        camPanUpPos = new Vector3(origin.x + 0.4f, origin.y + 7.1f, -10);// = new Vector3(-7.5f, 16f, -10f);

        //on enter this is the pose the camera happens to be at 
        //on exit this is the position the camera pans from. (same idea with the player start pose)
        camStartPos = new Vector3(origin.x + 0.4f, origin.y - 20, -10);
        playerStartPos = new Vector3(camStartPos.x, camStartPos.y + 0.5f, 0);

        //set the speed that the elevator panning happens at
        key7 = key6 + (camPanUpPos.y - camFrontofElevatorPos.y) / elevatorSpeed;
        key8 = 0.0f + (camFrontofElevatorPos.y - camStartPos.y) / elevatorSpeed;
        key9 = key9 + key8;
        key10 = key10 + key8;
        key11 = key11 + key8;
        key12 = key12 + key8;

        if (ElevatorAction == ElevatorActionType.ExitOnly
            || ElevatorAction == ElevatorActionType.EnterAndExit) {
            //immediately start the exit transition process

            exitInProgress = true;
            startTime = Time.time;

            //hide walls behind the elevator doors
            WallBehindLeftDoor.SetActive(false);
            WallBehindRightDoor.SetActive(false);

            //make the elevator doors appear in front of the player (for when they close)
            RightDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;
            LeftDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;

            //set the camera position to the starting position
            Camera.main.transform.position = camStartPos;

            //set the player position to the starting position
            player.transform.position = playerStartPos;

            player.GetComponent<Player>().pausePlayerControl();
            player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0); //set the sprite to invisible
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!entryInProgress && !exitInProgress) {
            if (ElevatorAction == ElevatorActionType.EnterOnly || ElevatorAction == ElevatorActionType.EnterAndExit) {
                if (collision.gameObject.name == "Player" && GameManager.Instance.hud.HasBattery)  {
                    //begin an entry animation

                    if (JanitorDialogOnEntry.Length > 0) {
                        GameManager.Instance.dialogueManager.Display(DialogueSpeaker.Janitor,
                            JanitorDialogOnEntry, 5);
                    }

                    //Start the entry transition process
                    entryInProgress = true;
                    startTime = Time.time;

                    playerStartPos = player.transform.position;
                    camStartPos = Camera.main.transform.position;
                    

                    //pause player inputs
                    player.GetComponent<Player>().pausePlayerControl();
                }
            }
        }
    }

    void TransitionComplete() {
        entryInProgress = false;
        exitInProgress = false;
        firstFrame = true;
        talkedYet = false;

        player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1); //set the sprite to visible
        player.GetComponent<Player>().resumePlayerControl();

    }

    // Update is called once per frame
    void Update() {
        Assert.IsFalse(entryInProgress && exitInProgress,
            "Can't be entering and exiting an elevator at the same time");

        if (entryInProgress) {
            PlayEntryTransition();
        }
        else if (exitInProgress) {
            PlayExitTransition();
        }
	}

    //played for the player entering the elevator
    void PlayEntryTransition() {
        float t = Time.time - startTime;

        if (t > 0 && t < key0)
        {
            float f = t / key0; //fraction of this interval for Lerp
            player.transform.position = Vector3.Lerp(playerStartPos, playerPlaceBatteryPos, f);
            player.GetComponent<Animator>().SetFloat("xvel", -0.5f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", 0.1f);
        }

        if (t > key0 && t < key1)
        {
            //initial set up (performed here so that the player doesn't accidentally clip behind the elevator doors)
            if (firstFrame)
            {
                //hide walls behind the elevator doors
                WallBehindLeftDoor.SetActive(false);
                WallBehindRightDoor.SetActive(false);

                //make the elevator doors appear in front of the player (for when they close)
                RightDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;
                LeftDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;

                //place the battery down
                batteryObj = Instantiate(batteryPrefab);
                batteryObj.SetActive(true);
                batteryObj.transform.parent = transform.parent;
                batteryObj.transform.position = batteryPlacePos;

                firstFrame = false;

                // Remove battery from HUD
                GameManager.Instance.hud.GiveBattery(false);
            }

            player.GetComponent<Animator>().SetFloat("xvel", 0.0f); //turn off player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", 0.0f);
        }

        if (t > key1 && t < key2)
        {
            float f = (t - key1) / (key2 - key1);
            player.transform.position = Vector3.Lerp(playerPlaceBatteryPos, playerFrontOfElevatorPos, f);
            Camera.main.transform.position = Vector3.Lerp(camStartPos, camFrontofElevatorPos, f);
            player.GetComponent<Animator>().SetFloat("xvel", 0.5f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", -0.1f);
        }

        if (t > key2 && t < key3)
        {
            float f = (t - key2) / (key3 - key2);
            float shake = 1 - 2 * Mathf.Pow(f - 0.5f, 1);

            LeftDoor.transform.position = LeftDoorInitPos + maxShakeOffset * shake * ((Vector3)Random.insideUnitCircle + Vector3.back * 10);
            LeftDoor.transform.rotation = Quaternion.Euler(0, 0, maxShakeAngle * shake * ((Random.value * 2) - 1));

            RightDoor.transform.position = RightDoorInitPos + maxShakeOffset * shake * ((Vector3)Random.insideUnitCircle + Vector3.back * 10);
            RightDoor.transform.rotation = Quaternion.Euler(0, 0, maxShakeAngle * shake * ((Random.value * 2) - 1));
        }

        if (t > key3 && t < key4)
        {
            float f = (t - key3) / (key4 - key3);
            LeftDoor.transform.position = Vector3.Lerp(LeftDoorInitPos, LeftDoorInitPos + Vector3.left * 0.8f, f);
            RightDoor.transform.position = Vector3.Lerp(RightDoorInitPos, RightDoorInitPos + Vector3.right * 0.8f, f);
        }

        if (t > key4 && t < key5)
        {
            float f = (t - key4) / (key5 - key4);
            player.transform.position = Vector3.Lerp(playerFrontOfElevatorPos, playerInsideElevatorPos, f);
        }

        if (t > key5 && t < key6)
        {
            float f = (t - key5) / (key6 - key5);
            LeftDoor.transform.position = Vector3.Lerp(LeftDoorInitPos + Vector3.left * 0.8f, LeftDoorInitPos, f);
            RightDoor.transform.position = Vector3.Lerp(RightDoorInitPos + Vector3.right * 0.8f, RightDoorInitPos, f);
        }

        if (t > key6 && t < key7)
        {
            float f = (t - key6) / (key7 - key6);
            Camera.main.transform.position = Vector3.Lerp(camFrontofElevatorPos, camPanUpPos, f);
            player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0); //set the sprite to invisible
            player.transform.position = Camera.main.transform.position + Vector3.up * 2.25f;
        }

        if (t > key7)
        {
            SoundManager.Instance.StopSoundTrack(1.5f);
            TransitionComplete();
            GameManager.Instance.levelManager.ImmediatelyLoadRoom(ElevatorToRoom, null);
        }
    }

    //played for the player exiting the elevator
    void PlayExitTransition() {
        float t = Time.time - startTime;

        //pan camera up to the elevator
        if (t > 0 && t < key8) {
            //Debug.Log(camStartPos);
            //Debug.Log(camFrontofElevatorPos);
            //Debug.Log(Camera.main.transform.position);
            //Debug.Break();

            float f = t / key8; //fraction of this interval for Lerp

            Camera.main.transform.position = Vector3.Lerp(camStartPos, camFrontofElevatorPos, f);
            player.transform.position = Vector3.Lerp(playerStartPos, playerInsideElevatorPos, f);
        }

        //pause for a moment
        if (t > key8 && t < key9) {
            //initial set up (performed here so that the player doesn't accidentally clip behind the elevator doors)
            if (firstFrame) {
                //turn player color back on.
                player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                firstFrame = false;
            }

            //force player position
            player.transform.position = playerInsideElevatorPos;
        }

        //open the elevator doors
        if (t > key9 && t < key10) {
            float f = (t - key9) / (key10 - key9);
            player.transform.position = playerInsideElevatorPos;
            LeftDoor.transform.position = Vector3.Lerp(LeftDoorInitPos, LeftDoorInitPos + Vector3.left * 0.8f, f);
            RightDoor.transform.position = Vector3.Lerp(RightDoorInitPos, RightDoorInitPos + Vector3.right * 0.8f, f);
        }

        //player walks out
        if (t > key10 && t < key11) {
            float f = (t - key10) / (key11 - key10);
            player.transform.position = Vector3.Lerp(playerInsideElevatorPos, playerFrontOfElevatorPos, f);
            player.GetComponent<Animator>().SetFloat("xvel", 0.0f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", -0.5f);

            if (JanitorDialogOnExit.Length > 0) {
                if (!talkedYet) {
                    GameManager.Instance.dialogueManager.Display(DialogueSpeaker.Janitor, JanitorDialogOnExit, 5);
                    talkedYet = true;
                }
            }
        }

        //doors close
        if (t > key11 && t < key12)  {
            float f = (t - key11) / (key12 - key11);
            LeftDoor.transform.position = Vector3.Lerp(LeftDoorInitPos + Vector3.left * 0.8f, LeftDoorInitPos, f);
            RightDoor.transform.position = Vector3.Lerp(RightDoorInitPos + Vector3.right * 0.8f, RightDoorInitPos, f);
            player.transform.position = playerFrontOfElevatorPos;
            player.GetComponent<Animator>().SetFloat("xvel", 0.0f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", 0.0f);
        }

        //clean up after completed transition
        if (t > key12) {
            //make walls collidable
            WallBehindLeftDoor.SetActive(true);
            WallBehindRightDoor.SetActive(true);

            //set elevator door layer to proper
            LeftDoor.GetComponent<SpriteRenderer>().sortingOrder = -1;
            RightDoor.GetComponent<SpriteRenderer>().sortingOrder = -1;

            player.GetComponent<Player>().resumePlayerControl();
            
            //start the next soundtrack
            SoundManager.Instance.StartSoundTrack(MusicInThisRoom);
            TransitionComplete();

        }
    }
}