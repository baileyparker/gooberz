using UnityEngine;


public class FadeToBlackTransition : BlackoutTransition {
    public FadeToBlackTransition(float duration, GameObject origin,
                                 GameObject destination, string entryDoor,
                                 GameObject transitionObject)
        : base(duration, origin, destination, entryDoor, transitionObject) {}

    protected override void Update(float percentComplete) {
        Blackout.SetActive(true);

        // TODO(bp): perhaps use tags, this is fragile
        CanvasRenderer rectangle =
            Blackout.transform.GetChild(0).GetComponent<CanvasRenderer>();
        rectangle.SetAlpha(1 - Mathf.Pow(2 * (percentComplete - 0.5f), 2));

        // Hold the player in the same spot
        Player.transform.position = PlayerStart;

        if (percentComplete > 0.5f) {
            // Swap visible room while screen is black
            Origin.SetActive(false);
            Destination.SetActive(false);

            Camera.transform.position = CameraEnd;
            Player.transform.position = PlayerEnd;
        }
    }
}
