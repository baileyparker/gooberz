using UnityEngine;


public abstract class Transition {
    private readonly float StartTime;
    protected readonly float Duration = 1.0f;

    protected readonly GameObject Origin;
    protected readonly GameObject Destination;
    protected readonly string EntryDoor;
    protected readonly GameObject TransitionObject;

    protected readonly GameObject Player;
    protected readonly Vector3 PlayerStart;
    protected readonly Vector3 PlayerEnd;

    protected Camera Camera { get { return Camera.main; } }
    protected readonly Vector3 CameraStart;
    protected readonly Vector3 CameraEnd;


    public Transition(float duration, GameObject origin, GameObject destination,
                      string entryDoor, GameObject transitionObject) {
        StartTime = Time.time;
        Duration = duration;

        Origin = origin;
        Destination = destination;
        EntryDoor = entryDoor;
        TransitionObject = transitionObject;

        Player = GameObject.Find("Player");
        PlayerStart = Player.transform.position;
        PlayerEnd = Destination.GetComponent<Room>().GetSpawnLocation(entryDoor);

        CameraStart = Camera.transform.position;
        CameraEnd = new Vector3(PlayerEnd.x, PlayerEnd.y, -10);
    }

    public bool Continue() {
        if (!IsComplete()) {
            Update((Time.time - StartTime) / Duration);
            return true;
        } else {
            Finish();
            return false;
        }
    }

    protected virtual void Update(float percentComplete) {}

    protected virtual void Finish() {
        // Clean up any camera rotations/zooms
        Camera.transform.transform.eulerAngles = new Vector3(0, 0, 0);
        Camera.orthographicSize = 5.0f;

        // Set final camera/player position
        Camera.transform.position = CameraEnd;
        Player.transform.position = PlayerEnd;

        // Trigger Room callbacks
        if (Origin != null) {
            Origin.GetComponent<Room>().OnExit();
            Origin.SetActive(false);
        }

        Destination.GetComponent<Room>().OnEnter(EntryDoor);
    }

    protected bool IsComplete() {
        return (Time.time - StartTime) > Duration;
    }
}
