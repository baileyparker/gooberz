using UnityEngine;


public class BatmanSpinTransition : ZoomInThenOutTransition {
    public readonly float Rotations;


    public BatmanSpinTransition(float duration, GameObject origin,
                                GameObject destination, string entryDoor,
                                GameObject transitionObject, float rotations)
        : base(duration, origin, destination, entryDoor, transitionObject) {
            Rotations = rotations;
    }

    protected override void Update(float percentComplete) {
        Camera.transform.transform.eulerAngles =
            new Vector3(0, 0, (Rotations * 360 * percentComplete) % 360);
        base.Update(percentComplete);
    }
}
