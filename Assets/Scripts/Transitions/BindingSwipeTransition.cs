using UnityEngine;


public class BindingSwipeTransition : Transition {
    public BindingSwipeTransition(float duration, GameObject origin,
                                  GameObject destination, string entryDoor,
                                  GameObject transitionObject)
        : base(duration, origin, destination, entryDoor, transitionObject) {}

    protected override void Update(float percentComplete) {
        Camera.transform.position = Vector3.Lerp(CameraStart, CameraEnd,
                                                 percentComplete);
        Player.transform.position = Vector3.Lerp(PlayerStart, PlayerEnd,
                                                 percentComplete);
    }
}
