using UnityEngine;


public class ZoomInThenOutTransition : BindingSwipeTransition {
    public ZoomInThenOutTransition(float duration, GameObject origin,
                                   GameObject destination, string entryDoor,
                                   GameObject transitionObject)
        : base(duration, origin, destination, entryDoor, transitionObject) {}

    protected override void Update(float percentComplete) {
        Camera.orthographicSize = 5.0f * Mathf.Pow(2 * (percentComplete - 0.5f), 2);
        base.Update(percentComplete);
    }
}
