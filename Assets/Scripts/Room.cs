﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;


public class Room : MonoBehaviour {
    public RoomName eastRoomName;
    public RoomName westRoomName;
    public RoomName northRoomName;
    public RoomName southRoomName;

    protected const float SPAWN_OFFSET = 1.1f;

    /// <summary>
    /// A room can be loaded with LevelManager.LoadRoom() with no entryDoor
    /// provided. In this case, we default to spawning the player in this
    /// location (because we don't have a door to place the player near).
    /// </summary>
    public Vector3 defaultSpawnLocation;

    /// <summary>
    /// Indicates whether enemies have already been spawned in this room. Used
    /// so the in non-RecordedRooms, enemies spawn only once (and don't spawn
    /// on re-entry). RecordedRooms override shouldSpawnEnemies() to always
    /// respawn.
    /// </summary>
    private bool alreadySpawned;


    public virtual void OnEnter(string entryDoor) {
        Player player = (Player) FindObjectOfType(typeof(Player));

        Vector3 spawnLocation = GetSpawnLocation(entryDoor);
        player.transform.position = spawnLocation;

        if (shouldSpawnEnemies()) {
            Debug.Log("Loading enemies");

            spawnEnemies();
            alreadySpawned = true;
        } else {
            Debug.Log("Not loading enemies");
        }
    }

    protected void spawnEnemies() {
        int count = 0;

        Transform[] children = gameObject.GetComponentsInChildren<Transform>();

        foreach (Transform child in children) {
            if (child.gameObject.CompareTag("EnemyLoader")) {
                EnemyLoader enemyLoader = child.GetComponent<EnemyLoader>();
                Assert.IsNotNull(enemyLoader, "EnemyLoader doesn't have script");

                enemyLoader.LoadEnemy();
                count++;
            }
        }

        Debug.Log("Spawned enemies: " + count);
    }

    protected virtual bool shouldSpawnEnemies() {
        return !alreadySpawned;
    }

    public Vector3 GetSpawnLocation(string entryDoorTag) {
        Debug.Log("Entering from: " + entryDoorTag);

        if (entryDoorTag == null) {
            return defaultSpawnLocation;
        }

        string parent = gameObject.name;
        GameObject door = GameObject.Find("/" + parent + "/" + entryDoorTag);
        Assert.IsNotNull(door, string.Format("Unable to find door {0} in room {1}",
                                             entryDoorTag, gameObject.name));
        Vector3 spawnLocation = door.transform.position;

        switch (entryDoorTag) {
            case "EastDoor":
                spawnLocation += SPAWN_OFFSET * Vector3.left;
                break;
            case "WestDoor":
                spawnLocation += SPAWN_OFFSET * Vector3.right;
                break;
            case "NorthDoor":
                spawnLocation += SPAWN_OFFSET * Vector3.down;
                break;
            case "SouthDoor":
                spawnLocation += SPAWN_OFFSET * Vector3.up;
                break;
            default:
                Assert.IsTrue(false, string.Format("bad door tag: {0}",
                                                   entryDoorTag));
                break;
        }

        return spawnLocation;
    }

    public virtual void OnExit() {
        destroyBullets();
    }

    private void destroyBullets() {
        foreach (GameObject bullet in GameObject.FindGameObjectsWithTag("Projectile")) {
            Destroy(bullet);
        }
    }
}
