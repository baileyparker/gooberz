﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSoundTrack : MonoBehaviour {
    void Start() {
        SoundManager.Instance.AttachSound(Camera.main.gameObject, SoundEnum.symphony);
    }
}
