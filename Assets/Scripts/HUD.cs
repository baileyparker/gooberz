using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using System.Collections.Generic;

public class HUD : MonoBehaviour {
    public Sprite EmptyHeart;
    public Sprite FullHeart;
    public Sprite[] gun_sprites;

    private GameObject watch;
    private GameObject watchbg;
    private GameObject[] hands;
    private GameObject[] hearts;
    private GameObject ammo;
    private GameObject ammoImage;
    private GameObject deathScreen;
    private GameObject battery;
    private GameObject key;
    private Animator animator;
    private Animator test0;
    private float angle = 0;

    private bool rewinding = false;
    private float rewindSpeed = 0;

    private bool _hasBattery;
    public bool HasBattery { get { return _hasBattery; } }

    public void Awake() {
        // Copy the gun sprites into the static array in Gun.cs
        // SB: TODO refactor this to be better
        Gun.sprites[0] = gun_sprites[0];
        Gun.sprites[1] = gun_sprites[1];
        Gun.sprites[2] = gun_sprites[2];
        Gun.sprites[3] = gun_sprites[3];
        Debug.Log("Gun statics set");

        watch = GameObject.FindWithTag("Watch");
        Assert.IsNotNull(watch, "couldn't find watch");

        watchbg = GameObject.FindWithTag("WatchBG");
        Assert.IsNotNull(watch, "couldn't find watch background");

        hands = GameObject.FindGameObjectsWithTag("WatchHand");
        Assert.IsTrue(hands.Length != 0, "couldn't find watch hands");

        hearts = GameObject.FindGameObjectsWithTag("HUDHeart");
        Assert.IsTrue(hearts.Length != 0, "couldn't find HUD hearts");

        ammo = GameObject.FindWithTag("HUDAmmo");
        Assert.IsNotNull(ammo, "couldn't find ammo");

        ammoImage = GameObject.FindWithTag("HUDAmmoImage");
        Assert.IsNotNull(ammo, "couldn't find ammo image");

        deathScreen = GameObject.FindWithTag("DeathScreen");
        Assert.IsNotNull(deathScreen, "couldn't find death screen");

        battery = gameObject.transform.Find("battery").gameObject;
        Assert.IsNotNull(battery, "couldn't find battery");

        animator = watchbg.GetComponent<Animator>();
        Assert.IsNotNull(animator, "couldn't find watch animator");


        key = gameObject.transform.Find("Key").gameObject;
        Assert.IsNotNull(battery, "couldn't find Key");

        HideWatch();
        HideDeathScreen();
    }

    public void Start() {
        GameManager.Instance.hud = this;
    }

    public void FixedUpdate() {
        if (watch.activeSelf) {
            if (!rewinding) {
                angle = (angle + 0.5f) % 4320f;
            } else {
                angle -= rewindSpeed;

                if (angle <= 0) {
                    rewinding = false;
                    angle = 0;
                }
            }

            hands[0].transform.rotation = Quaternion.Euler(0, 0, -angle);

            float bigHandAngle = (angle / 12f) % 360f;
            hands[1].transform.rotation = Quaternion.Euler(0, 0, -bigHandAngle);
        }
    }

    public void ResetWatch() {
        if (!rewinding) {
            rewinding = true;
            rewindSpeed = angle / (2f / Time.fixedDeltaTime);
        }
    }

    public void HideWatch() {
        watch.SetActive(false);
    }

    public void ShowWatch() {
        // TODO(BP): animate in watch
        watch.SetActive(true);
    }

    public void HideDeathScreen() {
		deathScreen.SetActive(false);
		animator.SetBool ("isShatter", false);
    }

    public void ShowDeathScreen() {
        deathScreen.SetActive(true);
    }

    public void SetPlayerHealth(int hp) {
        Assert.IsTrue(0 <= hp && hp <= 5, "hearts must be between 0 and 5");

        for (int i = 0; i < hearts.Length; i++) {
            string name = hearts[i].name;
            int index = int.Parse(name.Substring(name.Length - 1)) - 1;

            SetHeartFull(hearts[i], hp > index);
        }
    }

    protected void SetHeartFull(GameObject heart, bool isFull) {
        foreach (Image image in heart.GetComponentsInChildren<Image>()) {
            image.sprite = isFull ? FullHeart : EmptyHeart;
        }
    }

    public void SetPlayerAmmo(int bullets, Gun.Weapon weapon) {
        Text ammoText = ammo.GetComponent<Text>();
        if (bullets <= 0) {
            ammoText.text = "∞";
            Vector3 anchoredPos = ammoText.rectTransform.anchoredPosition3D;
            anchoredPos[1] = -186;
            ammoText.rectTransform.anchoredPosition3D = anchoredPos;
            SetImage(Gun.Weapon.Basic);
        } else {
            ammoText.text = "" + bullets;
            Vector3 anchoredPos = ammoText.rectTransform.anchoredPosition3D;
            anchoredPos[1] = -174;
            ammoText.rectTransform.anchoredPosition3D = anchoredPos;
            SetImage(weapon);
        }
    }

    public void GiveBattery(bool hasBattery) {
        _hasBattery = hasBattery;
        battery.SetActive(hasBattery);
    }

    public void GiveKey(bool hasKey) {
        key.SetActive(hasKey);
    }

    private void SetImage(Gun.Weapon weapon) {
        Image image = ammoImage.GetComponent<Image>();
        Sprite sprite;
        switch (weapon) {
            case Gun.Weapon.Basic:
                sprite = gun_sprites[0];
                break;
            case Gun.Weapon.Uzi:
                sprite = gun_sprites[1];
                break;
            case Gun.Weapon.Shotgun:
                sprite = gun_sprites[2];
                break;
            case Gun.Weapon.Laser:
                sprite = gun_sprites[3];
                break;
            default:
                sprite = gun_sprites[0];
                break;
        }
        image.sprite = sprite;
    }

    public void GameOver() {
        animator.SetBool ("isShatter", true);
        ShowDeathScreen();
        UpdateDeathScreen();
    }

    public void UpdateDeathScreen() {
        Text killedBy = GameObject.FindWithTag("KilledBy").GetComponent<Text>();
        killedBy.text = "Killed By: " + GameManager.Instance.statsManager.killedBy;

        Text enemiesKilled = GameObject.FindWithTag("EnemiesKilled").GetComponent<Text>();
        enemiesKilled.text = "Enemies Killed: " + GameManager.Instance.statsManager.enemiesKilled;

        Text shotsFired = GameObject.FindWithTag("ShotsFired").GetComponent<Text>();
        shotsFired.text = "Shots Fired: " + GameManager.Instance.statsManager.shotsFired;

        Text accuracy = GameObject.FindWithTag("Accuracy").GetComponent<Text>();
        float numShotsHit = GameManager.Instance.statsManager.shotsHit;
        float numShotsFired = GameManager.Instance.statsManager.shotsFired;
        int accuracyPercent = 0;
        if (numShotsFired != 0f) {
            accuracyPercent = (int) (numShotsHit / numShotsFired * 100);
        }
        string accuracyMessage = GetAccuracyMessage(accuracyPercent);
        accuracy.text = "Accuracy: " + accuracyPercent + "% "+ accuracyMessage;

        Text betrayals = GameObject.FindWithTag("Betrayals").GetComponent<Text>();
        betrayals.text = "Betrayals: " + GameManager.Instance.statsManager.betrayals;
    }

    private string GetAccuracyMessage(int accuracy) {
        if (accuracy < 10) {
            return "[Piss poor]";
        }
        if (accuracy < 20) {
            return "[Just terrible]";
        }
        if (accuracy < 30) {
            return "[Do you even aim bro?]";
        }
        if (accuracy < 40) {
            return "[Weak sauce]";
        }
        if (accuracy < 50) {
            return "[Where's the barn?]";
        }
        if (accuracy < 60) {
            return "[F-]";
        }
        if (accuracy < 70) {
            return "[A poor effort]";
        }
        if (accuracy < 80) {
            return "[C's get degrees!]";
        }
        if (accuracy < 90) {
            return "[You probably didn't shoot much]";
        }
        if (accuracy < 100) {
            return "[I'm mildly impressed]";
        }
        return "[You must have cheated]";
    }
}
