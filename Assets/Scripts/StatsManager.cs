﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour {
    public string killedBy;
    public int enemiesKilled = 0;
    public int shotsFired = 0;
    public int shotsHit = 0;
    public int betrayals = 0;

    public void IncrementShotsFired() {
        shotsFired++;
    }

    public void UpdateEnemyHit(int hp) {
        shotsHit++;
        if (hp <= 0) {
            enemiesKilled++;
        }
    }

    public void IncrementBetrayals() {
        betrayals++;
    }

    public void UpdatePlayerHit(int hp, EnemyInformation eInfo) {
        if (hp <= 0) {
            killedBy = eInfo.enemyName;
        }

        if (eInfo.enemyType == EnemyInformation.EnemyType.Ghost) {
            betrayals++;
        }
    }

    public void Reset() {
        killedBy = "";
        enemiesKilled = 0;
        shotsFired = 0;
        shotsHit = 0;
        betrayals = 0;
        GameManager.Instance.ScoreManager.Reset();
    }
}
