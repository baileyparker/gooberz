﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    private bool isLoading = false;
    [SerializeField]
    private Text loadingText;

    public void PlayGame() {
        isLoading = true;
        StartCoroutine(LoadNewScene());
    }

    public void QuitGame() {
        Debug.Log("Quit!");
        Application.Quit();
    }

    IEnumerator LoadNewScene() {
        // Start an asynchronous operation to load the scene that was passed to
        // the LoadNewScene coroutine.
        AsyncOperation async = SceneManager.LoadSceneAsync("CutScene");

        // While the asynchronous operation to load the new scene is not yet
        // complete, continue waiting until it's done.
        while (!async.isDone) {
            yield return null;
        }
    }

    public void Update () {
        if (isLoading) {
            // Pulse loading text to show something is happening/game isn't frozen
            loadingText.color = new Color (loadingText.color.r,
                                           loadingText.color.g,
                                           loadingText.color.b,
                                           Mathf.PingPong(Time.time, 1));
        }
    }
}
