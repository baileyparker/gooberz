using UnityEngine.Assertions;


public enum DialogueSpeaker {
    Janitor, President
}

public static class DialogueSpeakerExtensions {
    public static string Name(this DialogueSpeaker speaker) {
        switch (speaker) {
            case DialogueSpeaker.Janitor:
                return "Janitor";
            case DialogueSpeaker.President:
                return "President";
            default:
                Assert.IsTrue(false, "unknown dialogue speaker");
                return "<unknown>";
        }
    }
}
