using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This is used by the animator to tell the game object when it is time to
// perform its attack
abstract public class AttackingEnemy : MonoBehaviour {
    /// <summary>
    /// Flag for if this enemy has a frontswing for its attack
    /// </summary>
    public bool frontSwing;

    /// <summary>
    /// Flag for if this enemy has a backswing for its attack
    /// </summary>
    public bool backSwing;

    // Performs the actual attack logic (dealing damage, spawning bullets, etc.)
    abstract public void Attack();
}
