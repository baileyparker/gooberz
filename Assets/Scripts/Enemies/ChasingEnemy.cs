﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingEnemy : MonoBehaviour {

    /// <summary>
    /// A reference to the player's transform
    /// </summary>
    public Transform playerPos;

    /// <summary>
    /// The maximum velocity this enemy can move at
    /// </summary>
    public float maxVelocity;

    /// <summary>
    /// The damage this enemy does when hitting the player
    /// </summary>
    public int collisionDamage;

    /// <summary>
    /// A reference to the animation controller for this enemy
    /// Used to determine if we can move or not
    /// </summary>
    private Animator animator;

    /// <summary>
    /// The distance from the player at which the enemy will stop approaching.
    /// </summary>
    public int distance = 0;

    /// <summary>
    /// The enemy's life history
    /// </summary>
    public EnemyInformation enemyInformation;

    /// <summary>
    /// The duration to wait until beginning to chase the player on load
    /// </summary>
    public int snoozeTime = 0;

    // Use this for initialization
    void Awake() {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        playerPos = playerObject.transform;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        if (snoozeTime >= 0) {
            snoozeTime -= 1;
            return;
        }

        AnimatorStateInfo animationInfo;
        if (animator != null) {
            animationInfo = animator.GetCurrentAnimatorStateInfo(0);

            if (animationInfo.IsName("Stunned") ) {
                // If we're in the stunned state don't move
                GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                return;
            }
        }
        Vector3 direction = playerPos.position - transform.position;
        if (direction.magnitude < distance) {
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        } else {
            Vector3 nomralizedDirection = Vector3.Normalize(direction.NoZ());
            Vector3 delta = maxVelocity*nomralizedDirection;
            GetComponent<Rigidbody2D>().velocity = delta;
        }
    }

    protected void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag.Contains("Player")) {
            Player player = other.gameObject.GetComponent<Player>();
            int startHP = player.hp;
            player.LoseHP(collisionDamage);
            if (player.hp < startHP) {
                 GameManager.Instance.statsManager.UpdatePlayerHit(player.hp, enemyInformation);
            }
            player.CheckGameover();
        }
    }
}
