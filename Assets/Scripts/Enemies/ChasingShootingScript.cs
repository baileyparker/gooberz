﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingShootingScript : MonoBehaviour {

	/// <summary>
    /// A reference to the player's transform
    /// </summary>
    public Transform playerPos;

	///<summary>
	/// A reference to the player's velocity
	/// </summary>
	public Vector3 playerVel;

    /// <summary>
    /// The maximum velocity this enemy can move at
    /// </summary>
    public int maxVelocity;

    /// <summary>
    /// The damage this enemy does when hitting the player
    /// </summary>
    public int collisionDamage;

     /// <summary>
    /// The projectile launched by the enemy's shoot action.
    /// </summary>
    public GameObject projectile;

    /// <summary>
    /// The speed of the projectile launched by the enemy.
    /// </summary>
    public float projectile_speed;

    /// <summary>
    /// The frequency with which the enemy shoots.
    /// </summary>
    public int freq;

    /// <summary>
    /// The distance from a player a shooting enemy will stand.
    /// </summary>
    public int distance;

    /// <summary>
    /// The frequency with which the enemy shoots.
    /// </summary>
    [SerializeField]
    private int time_since_shot;

    /// <summary>
    /// A reference to this object's animation controller.
    /// </summary>
    protected Animator animator;

    /// <summary>
    /// The enemy's life history
    /// <summary>
    public EnemyInformation enemyInformation;

    // Use this for initialization
    void Awake() {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        playerPos = playerObject.transform;
		playerVel = playerObject.GetComponent<Rigidbody2D> ().velocity;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
		Vector3 direction = playerPos.position - transform.position + playerVel;
		Vector3 normalizedDirection = Vector3.Normalize(direction.NoZ());
        Vector3 delta = maxVelocity*normalizedDirection;
        GetComponent<Rigidbody2D>().velocity = delta;
        
        if (direction.magnitude < distance) {
        	GetComponent<Rigidbody2D>().velocity = new Vector3();
        }

        time_since_shot++;
        if (time_since_shot == (freq - 10)) {
            if (animator != null) {
                animator.SetTrigger("attack");
            }
        }
        if (time_since_shot < freq) {
        	return;
        }

        FireShot(normalizedDirection);
        time_since_shot = 0;
    }

    protected void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag.Contains("Player")) {
            Player player = other.gameObject.GetComponent<Player>();
            int startHP = player.hp;
            player.LoseHP(collisionDamage);
            if (player.hp < startHP) {
                 GameManager.Instance.statsManager.UpdatePlayerHit(player.hp, enemyInformation);
            }
            player.CheckGameover();
        }
    }

    private void FireShot(Vector3 direction) {      
        Vector3 position = transform.position + direction;

        Vector3 velocity;
        GameObject bullet = (GameObject)Instantiate(projectile,
                                                    position,
                                                    Quaternion.identity);
        velocity = GetVelocity(position);
        bullet.GetComponent<Rigidbody2D>().velocity = velocity;
        bullet.GetComponent<EnemyProjectile>().enemyInformation = enemyInformation;
    }

    // TODO (SB): this is awful, find a better way ASAP
    private Vector3 GetVelocity(Vector3 vec) {
        Vector3 velocity = new Vector3(vec.x - transform.position.x,
                           vec.y - transform.position.y,
                           0);
        return Vector3.ClampMagnitude(100*velocity, projectile_speed);
    }
}
