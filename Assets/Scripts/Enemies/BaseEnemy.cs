using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

public class BaseEnemy : MonoBehaviour {
    /// <summary>
    /// The enemy's current health points.
    /// </summary>
    public int hp = 1;

    /// <summary>
    /// A reference to the animation controller for this enemy
    /// </summary>
    private Animator animator;


    void Awake() {
        // TODO (SB) the thing Bailey mentioned with
        // enforcing there is an animator
        animator = GetComponent<Animator>();
    }

    public bool LoseHP(int loss) {
        hp -= loss;
        if (hp <= 0) {
            Assert.IsTrue(Die());
        }
        // Trigger the animation
        if (animator != null) {
            animator.SetTrigger("takeDamage");
        }
        return true;
    }

    public bool Die() {
        Destroy(gameObject);
        GameManager.Instance.ScoreManager.AddScore(100);
        return true;
    }
}
