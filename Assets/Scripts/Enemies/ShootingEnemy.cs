using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : AttackingEnemy {

    /// <summary>
    /// A reference to this object's animation controller.
    /// </summary>
    protected Animator animator;

    /// <summary>
    /// A reference to the player's transform
    /// </summary>
    protected Transform playerPos;

    /// <summary>
    /// The projectile launched by the enemy's shoot action.
    /// </summary>
    public GameObject projectile;

    /// <summary>
    /// The speed of the projectile launched by the enemy.
    /// </summary>
    public float projectile_speed;

    /// <summary>
    /// The frequency with which the enemy shoots.
    /// </summary>
    public int freq;

    /// <summary>
    /// The time since this unit's last shot.
    /// </summary>
    [SerializeField]
    protected int time_since_shot;

    /// <summary>
    /// The enemy's life history
    /// <summary>
    public EnemyInformation enemyInformation;

    void Awake() {
        // Grab the player
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        playerPos = playerObject.transform;
        // Grab the animator
        animator = GetComponent<Animator>();

    }

    void Start() {
        // Find a reference to the StateMachineBehaviour in Start since it
        // might not exist yet in Awake.
        AttackAnimatorBehaviour smb = animator.GetBehaviour <AttackAnimatorBehaviour> ();

        // Set the StateMachineBehaviour's reference to this.
        smb.enemyScript = this;
    }

    void Update() {
        time_since_shot++;
        //TODO (SB): replace this with actual time based stuff or something
        //better than frame counts
        if (time_since_shot < freq) {
            return;
        }

        LaunchAttack();
        time_since_shot = 0;
    }

    protected void LaunchAttack() {
        if (frontSwing) {
            animator.SetTrigger("attackFrontSwing");
        } else {
            animator.SetTrigger("attack");
        }
    }

    override public void Attack() {
		GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
		Vector3 playerVel = playerObject.GetComponent<Rigidbody2D> ().velocity;
		Vector3 attack_direction = playerPos.position - transform.position + playerVel;
        Vector3 velocity = Vector3.Normalize(attack_direction.NoZ());

        Vector3 position = transform.position;

        GameObject bullet = (GameObject)Instantiate(projectile,
                                                    position,
                                                    Quaternion.identity);

        bullet.GetComponent<Rigidbody2D>().velocity = velocity * projectile_speed;
        bullet.GetComponent<EnemyProjectile>().enemyInformation = enemyInformation;
    }

    // TODO (SB): this is awful, find a better way ASAP
    private Vector3 GetVelocity(Vector3 vec) {
        Vector3 velocity = new Vector3(vec.x - transform.position.x,
                           vec.y - transform.position.y,
                           0);
        return Vector3.ClampMagnitude(100*velocity, projectile_speed);
    }
}
