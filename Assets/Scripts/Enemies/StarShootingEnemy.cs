﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarShootingEnemy : ShootingEnemy {

    /// <summary>
    /// Distance to spawn the bullets at
    /// </summary>
    protected float offset = 0.32f;

    override public void Attack() {
        Vector3[] positions = ProjectileLocations();

        Vector3 velocity;
        foreach(Vector3 vec in positions) {
            GameObject bullet = (GameObject)Instantiate(projectile,
                                                        vec,
                                                        Quaternion.identity);
            velocity = GetVelocity(vec);
            bullet.GetComponent<Rigidbody2D>().velocity = velocity;
            bullet.GetComponent<EnemyProjectile>().enemyInformation = enemyInformation;
        }
    }

    // TODO (SB): this is awful, find a better way ASAP
    private Vector3 GetVelocity(Vector3 vec) {
        Vector3 velocity = new Vector3(vec.x - transform.position.x,
                           vec.y - transform.position.y,
                           0);
        return Vector3.Normalize(velocity)*projectile_speed;
    }

    private Vector3[] ProjectileLocations() {
        Vector3[] positions = new Vector3[8];
        positions[0] = new Vector3(transform.position.x + offset,
                                   transform.position.y,
                                   transform.position.z);
        positions[1] = new Vector3(transform.position.x - offset,
                                   transform.position.y,
                                   transform.position.z);
        positions[2] = new Vector3(transform.position.x,
                                   transform.position.y + offset,
                                   transform.position.z);
        positions[3] = new Vector3(transform.position.x,
                                   transform.position.y - offset,
                                   transform.position.z);
        positions[4] = new Vector3(transform.position.x + offset,
                                   transform.position.y + offset,
                                   transform.position.z);
        positions[5] = new Vector3(transform.position.x - offset,
                                   transform.position.y - offset,
                                   transform.position.z);
        positions[6] = new Vector3(transform.position.x + offset,
                                   transform.position.y - offset,
                                   transform.position.z);
        positions[7] = new Vector3(transform.position.x - offset,
                                   transform.position.y + offset,
                                   transform.position.z);
        return positions;

    }
}
