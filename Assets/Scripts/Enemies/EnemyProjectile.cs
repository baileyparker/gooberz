﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : Projectile {
	public EnemyInformation enemyInformation;

    protected override void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "Player") {
            Player player = coll.gameObject.GetComponent<Player>();
            int startHP = player.hp;
            player.LoseHP(damage);
            if (player.hp < startHP) {
                 GameManager.Instance.statsManager.UpdatePlayerHit(player.hp, enemyInformation);
            }
            player.CheckGameover();
        }

        Destroy(gameObject);
    }
}

