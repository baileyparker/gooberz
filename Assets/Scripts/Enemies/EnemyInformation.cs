﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyInformation {

	public enum EnemyType { Slime, Flame, Bat, Ghost }
	public EnemyType enemyType;
	public string enemyName;
}
