﻿using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;


public abstract class RandomSprite : MonoBehaviour {
    public Sprite[] sprites;

    void Awake() {
        Assert.IsTrue(sprites.Length > 0, "no sprites to choose from");

        // Choose a random sprite to render
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = sprites[0];
    }
}
