using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {
    // Singleton
    private static GameManager _instance;

    public static GameManager Instance {
        get { return GameManager._instance; }
    }

    // TODO(bp): Wiring is inconsistent here
    public SoundManager soundManager;
    public HUD hud;

    [HideInInspector]
    public LevelManager levelManager;

    [HideInInspector]
    public DialogueManager dialogueManager;

    [HideInInspector]
    public StatsManager statsManager;

    [HideInInspector]
    public Player Player;

    [HideInInspector]
    public ScoreManager ScoreManager;


    void Awake() {
        // Setup singleton
        if (GameManager.Instance == null) {
            GameManager._instance = this;
        }

        levelManager =
            (LevelManager) GameObject.FindObjectOfType(typeof(LevelManager));
        Assert.IsNotNull(levelManager,
                         "cannot find level manager in Main Scene");

        // Better to have failed asserts get noticed
        Assert.raiseExceptions = true;

        dialogueManager =
            (DialogueManager) GameObject.FindObjectOfType(typeof(DialogueManager));
        dialogueManager.gameObject.SetActive(false);
        Assert.IsNotNull(dialogueManager,
                         "cannot find dialogue manager in Main Scene camera");

        statsManager =
            (StatsManager) GameObject.FindObjectOfType(typeof(StatsManager));
        Assert.IsNotNull(statsManager,
                        "cannot find stats manager in Main Scene");

        Player = (Player) GameObject.FindObjectOfType(typeof(Player));
        Assert.IsNotNull(Player, "cannot find player in Main Scene");

        ScoreManager = (ScoreManager) GameObject.FindObjectOfType(typeof(ScoreManager));
        Assert.IsNotNull(ScoreManager, "cannot find score manager in Main Scene");
    }

    public void Update() {
        if (Input.GetKeyUp(KeyCode.Alpha0)) {
            levelManager.ImmediatelyLoadRoom(RoomName.Level0Room1, null);
        } else if (Input.GetKeyUp(KeyCode.Alpha1)) {
            levelManager.ImmediatelyLoadRoom(RoomName.Level1Room1, null);
        } else if (Input.GetKeyUp(KeyCode.Alpha2)) {
            levelManager.ImmediatelyLoadRoom(RoomName.Level2Room1, null);
        } else if (Input.GetKeyUp(KeyCode.Escape)) {
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void GameOver() {
        hud.GameOver();
        levelManager.GameOver();
    }
}
