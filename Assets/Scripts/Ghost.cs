using UnityEngine;
using System.Collections.Generic;

public class Ghost : BasePlayer {
    public RecordedInput recording;
    public EnemyInformation enemyInformation;

    public override void Start() {
        base.Start();

        recording.OnFinish(delegate() {
            transform.position = recording.StartingPosition;
            recording.Rewind();
        });
    }

    protected override void OnEnterDoor(string tag) {}

    protected override IPlayerInputSource GetInputSource() {
        return recording;
    }

    protected override GameObject shootBasic(PlayerInput input) {
        GameObject bullet = base.shootBasic(input);
        if (decAmmo) {
            decAmmo = false;
            bullet.GetComponent<GhostProjectile>().enemyInformation = enemyInformation;
        }
        return null;
    }

    protected override List<GameObject> shootShotgun(PlayerInput input) {
        List<GameObject> bullets = base.shootShotgun(input);
        foreach (GameObject bullet in bullets) {
            bullet.GetComponent<GhostProjectile>().enemyInformation = enemyInformation;
        }
        return null;
    }

    protected override GameObject shootUzi(PlayerInput input) {
        GameObject bullet = base.shootUzi(input);
        if (decAmmo) {
            decAmmo = false;
            bullet.GetComponent<GhostProjectile>().enemyInformation = enemyInformation;
        }
        return null;
    }

    protected override GameObject shootLaser(PlayerInput input) {
        GameObject bullet = base.shootLaser(input);
        if (decAmmo) {
            decAmmo = false;
            bullet.GetComponent<GhostProjectile>().enemyInformation = enemyInformation;
        }
        return null;
    }
}
