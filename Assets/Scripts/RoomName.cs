using System;

using UnityEngine.Assertions;


public enum RoomName {
    Nowhere = 0,

    // ** IMPORTANT: READ BEFORE CHANGING THIS FILE **
    //
    // To add a room, add it to this list (in sorted order!).
    // It should be of the format LevelXRoomY where X is a 1-digit number and
    // Y is a n-digit number (with no leading zeros).
    //
    // This should correspond to: Prefabs/Resources/Level X/Room Y.prefab.
    // Note the spaces and capitalization!
    //
    // For convention, please number rooms starting at 1.
    //
    // Each item must have a UNIQUE integer value. ONCE A NUMBER HAS BEEN
    // ASSIGNED IT MUST NOT BE CHANGED! This means if you need to add a room
    // to the middle of the list, you must choose the next integer value (even
    // if this means the integers will be out of order) AND YOU CANNOT CHANGE
    // THE VALUES OF ANY OTHER ROOMS. IF YOU DON'T FOLLOW THESE INSTRUCTIONS,
    // YOU WILL BREAK EVERYTHING AND I WILL THROW THINGS AT YOU!
    //
    // When you add a new room, please update the comment at the bottom of this
    // enum indicating the next value number to use. And ALWAYS USE THIS VALUE
    // when inserting a new room!

    Level0Room1 = 1,
    Level0Room2 = 2,
    Level0Room3 = 3,
    Level0Room4 = 4,
    Level0Room5 = 5,
    Level0Room6 = 6,
    Level1Room1 = 7,
    Level1Room2 = 8,
    Level1Room3 = 9,
    Level1Room4 = 10,
    Level1Room5 = 11,
    Level1Room6 = 28,
    Level1Room7 = 29,
    Level2Room1 = 12,
    Level2Room2 = 13,
    Level2Room3 = 14,
    Level2Room4 = 15,
    Level2Room5 = 16,
    Level2Room6 = 17,
    Level2Room7 = 18,
    Level2Room8 = 19,
    Level2Room9 = 20,
    Level2Room10 = 21,
    Level2Room11 = 22,
    Level2Room12 = 23,
    Level2Room13 = 24,
    Level3Room1 = 25,
    Level3Room2 = 26,
    Level3Room3 = 27,
    Level3Room4 = 30,
    Level3Room5 = 31,
    Level3Room6 = 32,
    Level3Room7 = 33,
    Level3Room8 = 34,
    Level3Room9 = 35,
    Level3Room10 = 36,

    // NEXT VALUE: 37
    // PLEASE KEEP THIS UP TO DATE!
}

public static class RoomNameExtensions {
    public static string ResourceName(this RoomName room) {
        Assert.IsFalse(room == RoomName.Nowhere,
                       "can't get resource name for nowhere!");

        return "Level " + room.GetLevelNum() + "/Room " + room.GetRoomNum();
    }

    // NOTE: These are dependent on the naming scheme above!
    public static RoomName LevelStart(this RoomName room) {
        Assert.IsFalse(room == RoomName.Nowhere,
                       "can't get level start for nowhere!");

        string name = "Level" + room.GetLevelNum() + "Room1";
        return (RoomName) Enum.Parse(typeof(RoomName), name);
    }

    private static string GetLevelNum(this RoomName room) {
        Assert.IsFalse(room == RoomName.Nowhere,
                       "can't get level num for nowhere!");

        return room.ToString().Substring(5, 1);
    }

    private static string GetRoomNum(this RoomName room) {
        Assert.IsFalse(room == RoomName.Nowhere,
                       "can't get room num for nowhere!");

        return room.ToString().Substring(10);
    }
}
