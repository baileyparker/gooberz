using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;

abstract public class BasePlayer : MonoBehaviour {
    /// <summary>
    /// The projectile launched by the player's shoot action.
    /// </summary>
    public GameObject projectile;

    /// <summary>
    /// The player's current health points.
    /// </summary>
    public int hp = 5;

    /// <summary>
    /// Whether or not the player has a key.
    /// </summary>
    public bool hasKey = false;

    /// <summary>
    /// The players max hit points.
    /// <summary>
    private const int MAX_HP = 5;

    /// <summary>
    /// The number of invincibility frames the player receives when hit.
    /// </summary>
    public const int iFramesOnHit = 60;

    /// <summary>
    /// Number of invincibility frames that have passed.
    /// </summary>
    private int currentIFrames = 0;

    private IPlayerInputSource inputSource;

    /// <summary>
    /// When this is false (e.g. during cutscenes) all player input is ignored
    /// </summary>
    private bool inputAllowed = true;

    public bool gameover;

    /// <summary>
    /// The sound manager.
    /// </summary>
    // TODO(BP): Should normally be attached to something higher up...
    //public SoundManager SoundManager;


    private Animator animator;
    protected Gun.Weapon currentWeapon;
    protected int ammo = 0;
    protected bool decAmmo;

    [SerializeField]
    private float projectileVelocity;
    private float moveSpeed;
    private int projectileDamage;

    private float decay;
    private float delayPistol = 0f;
    private float delayShotgun = 0f;
    private float currTime;

    public virtual void Start() {
        this.inputSource = GetInputSource();
        projectileDamage = 1;
        decay = .1f;
        currentWeapon = Gun.Weapon.Basic;
        animator = GetComponent<Animator>();
    }

    public void FixedUpdate() {
        if (!gameover) {
            if (inputSource == null) {
                return;
            }

            PlayerInput input;
            if (inputAllowed) {
                input = inputSource.GetInput();
            }
            else {
                input = new PlayerInput(Vector2.zero, new bool[] {false, false, false, false, false, false, false, false}, false);
            }

            if (input != null) {
                
                GetComponent<Rigidbody2D>().velocity = input.velocity * 5f;

                animator.SetFloat("xvel", input.velocity.x);
                animator.SetFloat("yvel", input.velocity.y);

                // Play walking sound
                if (input.velocity != Vector2.zero) {
                    OnStartWalking();
                } else {
                    OnStopWalking();
                }

                if (input.cycleWeapon) {
                    currentWeapon += 1;
                    if ((int)currentWeapon == 4) {
                        currentWeapon = Gun.Weapon.Basic;
                    }
                }

                // Shoot single shot weapons
                if (input.shoot || input.shootHold) {
                    if (currentWeapon == Gun.Weapon.Basic) {
                        shootBasic(input);
                    }

                    if (currentWeapon == Gun.Weapon.Shotgun) {
                        shootShotgun(input);
                    }
                    if (currentWeapon == Gun.Weapon.Uzi) {
                        shootUzi(input);
                    }

                    if (currentWeapon == Gun.Weapon.Laser) {
                        shootLaser(input);
                    }
                }

                if (ammo <= 0 || input.AmmoDepleted) {
                    ammo = 1;
                    currentWeapon = Gun.Weapon.Basic;
                    input.AmmoDepleted = true;
                }

                // TODO(BP): this should be an animation state (we shouldn't be counting
                //           frames here)
                if (currentIFrames > 0) {
                    currentIFrames--;
                }

                if (currentIFrames == 0) {
                    animator.SetBool("invul", false);
                }
            }
        }
    }

    protected virtual void OnStartWalking() {}
    protected virtual void OnStopWalking() {}

    // TODO(PT): add y axis shift

    private Vector2 projectileVelDir(PlayerInput input) {
        if (input.fireUp) {
            return transform.up;
        } else if (input.fireLeft) {
            return -1 * transform.right;
        } else if (input.fireDown) {
            return -1 * transform.up;
        } else if (input.fireRight) {
            return transform.right;
        } else {
            return new Vector2(0,0);
        }
    }

    private Vector2 projectileVelShift(PlayerInput input) {
        if (input.fireUp) {
            return -1 * transform.right;
        } else if (input.fireLeft) {
            return -1 * transform.up;
        } else if (input.fireDown) {
            return transform.right;
        } else if (input.fireRight) {
            return transform.up;
        } else {
            return new Vector2(0,0);
        }
    }

    protected virtual GameObject shootBasic(PlayerInput input) {
        float deltaTime = Time.time - currTime;
        delayPistol -= deltaTime;
        currTime = Time.time;
        if (delayPistol <= 0) {
            decAmmo = true;
            Vector2 velDir = projectileVelDir (input);
            velDir = velDir * 0.8f;

            Vector2 smallVelDir = velDir * 0.2f;

            Vector3 projSpawnloc = new Vector3 (transform.position.x + smallVelDir.x, transform.position.y + smallVelDir.y, transform.position.z);

            GameObject bullet1 = (GameObject)Instantiate (projectile, projSpawnloc, Quaternion.identity);

            bullet1.GetComponent<Rigidbody2D>().AddForce(velDir * projectileVelocity, ForceMode2D.Impulse);
            bullet1.GetComponent<Projectile>().damage = projectileDamage;
            delayPistol = 0.4f;
            return bullet1;
        }
        return null;
    }

    protected virtual List<GameObject> shootShotgun(PlayerInput input) {
        float deltaTime = Time.time - currTime;
        delayShotgun -= deltaTime;
        currTime = Time.time;
        if (delayShotgun <= 0) {
            decAmmo = true;
            ammo--;
            GameManager.Instance.hud.SetPlayerAmmo(ammo, currentWeapon);

            Vector2 velDir = projectileVelDir (input);
            Vector2 smallVelDir = velDir * 0.2f;
            Vector2 velShift = projectileVelShift (input);
            Vector3 projSpawnloc = new Vector3 (transform.position.x + smallVelDir.x, transform.position.y + smallVelDir.y, transform.position.z);
            Vector3 projSpawnloc2 = new Vector3 (transform.position.x + smallVelDir.x, transform.position.y + smallVelDir.y + velShift.y * .3f, transform.position.z);
            Vector3 projSpawnloc3 = new Vector3 (transform.position.x + smallVelDir.x, transform.position.y + smallVelDir.y - velShift.y * .3f, transform.position.z);

            GameObject bullet1 = (GameObject)Instantiate (projectile, projSpawnloc, Quaternion.identity);
            GameObject bullet2 = (GameObject)Instantiate (projectile, projSpawnloc2, Quaternion.identity);
            GameObject bullet3 = (GameObject)Instantiate (projectile, projSpawnloc3, Quaternion.identity);

            bullet1.GetComponent<Projectile>().damage = projectileDamage;
            bullet2.GetComponent<Projectile>().damage = projectileDamage;
            bullet3.GetComponent<Projectile>().damage = projectileDamage;
            bullet1.GetComponent<Rigidbody2D>().AddForce(velDir * projectileVelocity, ForceMode2D.Impulse);
            bullet2.GetComponent<Rigidbody2D>().AddForce(velDir * projectileVelocity, ForceMode2D.Impulse);
            bullet2.GetComponent<Rigidbody2D>().AddForce(velShift * 6, ForceMode2D.Impulse);
            bullet3.GetComponent<Rigidbody2D>().AddForce(velDir * projectileVelocity, ForceMode2D.Impulse);
            bullet3.GetComponent<Rigidbody2D>().AddForce(velShift * -6, ForceMode2D.Impulse);
            List<GameObject> bullets = new List<GameObject>();
            bullets.Add(bullet1);
            bullets.Add(bullet2);
            bullets.Add(bullet3);
            delayShotgun = 0.5f;
            return bullets;
        }
        return null;
    }

    protected virtual GameObject shootUzi(PlayerInput input) {
        Vector2 velDir = projectileVelDir (input);
        Vector2 smallVelDir = velDir*0.2f;
        Vector2 velShift = projectileVelShift (input);

        if (decay > 0) {
            decay -= Time.deltaTime;
            return null;
        } else {
            decAmmo = true;
            ammo--;
            GameManager.Instance.hud.SetPlayerAmmo(ammo, currentWeapon);
            Vector3 projSpawnloc = new Vector3 (transform.position.x + smallVelDir.x, transform.position.y + smallVelDir.y, transform.position.z);

            GameObject bullet1 = (GameObject)Instantiate (projectile, projSpawnloc, Quaternion.identity);

            bullet1.GetComponent<Projectile>().damage = projectileDamage;
            bullet1.GetComponent<Rigidbody2D>().AddForce(velDir * projectileVelocity, ForceMode2D.Impulse);
            bullet1.GetComponent<Rigidbody2D>().AddForce(velShift * Random.Range (-5f, 5f), ForceMode2D.Impulse);

            decay = .1f;
            return bullet1;
        }
    }

    protected virtual GameObject shootLaser(PlayerInput input) {
        Vector2 velDir = projectileVelDir (input);
        Vector2 smallVelDir = velDir*0.2f;
        if (decay > 0) {
            decay -= Time.deltaTime;
            return null;
        } else {
            decAmmo = true;
            ammo--;
            GameManager.Instance.hud.SetPlayerAmmo(ammo, currentWeapon);
            Vector3 projSpawnloc = new Vector3 (transform.position.x + smallVelDir.x, transform.position.y + smallVelDir.y, transform.position.z);

            GameObject bullet1 = (GameObject)Instantiate (projectile, projSpawnloc, Quaternion.identity);

            bullet1.GetComponent<Projectile>().damage = projectileDamage;
            bullet1.GetComponent<Rigidbody2D>().AddForce(velDir * projectileVelocity, ForceMode2D.Impulse);

            decay = .03f;
            return bullet1;
        }
    }

    protected void OnTriggerEnter2D(Collider2D other) {
        if (other.tag.Contains("Door")) {
            OnEnterDoor(other.tag);
        } else if (other.tag.Contains("Battery")) {
            OnCollectBattery(other.transform.gameObject);
        } else if (other.tag.Equals("HealthPack")) {
            GainHP(other.gameObject.GetComponent<HealthPack>().healthRecovered);
            other.gameObject.SetActive(false);
        } else if (other.tag.Equals("Gun")) {
            OnCollectGun(other.gameObject);
        } else if (other.tag.Equals("Key")) {
            OnCollectKey();
            other.gameObject.SetActive(false);
        }
    }

    protected void OnCollectKey() {
        this.hasKey = true;
    }

    protected virtual void OnCollectBattery(GameObject battery) {
        GameManager.Instance.hud.GiveBattery(true);
        Destroy(battery);
    }

    protected virtual void OnCollectGun(GameObject gun) {
        Gun gunScript = gun.GetComponent<Gun>();
        ammo = gunScript.ammo;
        currentWeapon = gunScript.weapon;
        GameManager.Instance.hud.SetPlayerAmmo(ammo, currentWeapon);
    }

    abstract protected void OnEnterDoor(string tag);
    abstract protected IPlayerInputSource GetInputSource();

    // Return 0 if successful, 1 if fail
    // Fail happens when player is invulnerable
    public int LoseHP(int loss) {
        if (currentIFrames > 0) {
            return 1;
        }

        hp -= loss;
        GameManager.Instance.hud.SetPlayerHealth(hp);

        animator.SetBool("invul", true);
        currentIFrames = iFramesOnHit;
        return 0;
    }

    public void GainHP(int gain) {
        hp += gain;

        if (hp > MAX_HP) {
            hp = MAX_HP;
        }

        GameManager.Instance.hud.SetPlayerHealth(hp);
    }

    public virtual void CheckGameover() {
        if (hp <= 0) {
            gameover = true;
            GameManager.Instance.GameOver();
        }
    }

    public void Reset() {
        GainHP(MAX_HP);
        GameManager.Instance.hud.GiveBattery(false);
        gameover = false;
        currentIFrames = 0;
        ammo = 0;
        GameManager.Instance.hud.SetPlayerAmmo(ammo, currentWeapon);
        hasKey = false;
    }

    public void pausePlayerControl() {
        inputAllowed = false;
        gameObject.GetComponent<Collider2D>().enabled = false;
    }
    public void resumePlayerControl() {
        inputAllowed = true;
        gameObject.GetComponent<Collider2D>().enabled = true;
    }
}
