using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;

public class RecordedRoom : Room {
    /// <summary>
    /// The prefab to spawn as the player's ghost (the recorded versions of
    /// the player's inputs from previous visits to a room).
    /// </summary>
    public GameObject ghost;

    /// <summary>
    /// Recordings of user input from previous "runs" in this room.
    /// </summary>
    private List<RecordedInput> recordings = new List<RecordedInput>();

    /// <summary>
    /// Ghosts currently replaying the players previous "runs" in this room.
    /// These should be destroyed and emptied every time the player leaves the
    /// room.
    /// </summary>
    private List<GameObject> ghosts = new List<GameObject>();

    /// <summary>
    /// The recorder that collects inputs from the players current "run" in
    /// this room. Produces a recording that can be fed to a ghost to replay
    /// when the player returns to this room.
    /// </summary>
    private InputRecorder recorder;

    /// <summary>
    /// The minimum number of seconds a player must be in a room before the
    /// recording for that "run" is saved. Set this high enough to prevent
    /// quick jumps between doors from creating lots of short, useless
    /// recordings.
    /// </summary>
    private float minRecordingThreshold = 2f;


    public void Awake() {
        Assert.IsNotNull(ghost, "RecordedRoom requires a ghost");
    }

    public override void OnEnter(string entryDoor) {
        base.OnEnter(entryDoor);

        // Start recording for ghost created from this "run" of the room
        Vector3 spawnLocation = GetSpawnLocation(entryDoor);
        recorder = new InputRecorder(new KeyboardInput(), spawnLocation);

        StartCoroutine("SpawnGhosts");

        // TODO(bp): investigate why this must be guarded for directly
        //           loading rooms (not by entering a door)
        if (entryDoor != null) {
            // Reset the watch in the HUD
            GameManager.Instance.hud.ResetWatch();
        }
    }

    protected IEnumerator SpawnGhosts() {
        yield return new WaitForSeconds(1f);

        // Spawn ghosts from previous "runs" in the room
        foreach (RecordedInput recording in recordings) {
            GameObject newGhost = Instantiate(ghost, recording.StartingPosition,
                                              Quaternion.identity);
            newGhost.GetComponent<Ghost>().recording = recording;
            ghosts.Add(newGhost);
        }
    }

    protected override bool shouldSpawnEnemies() {
        return true;
    }

    public void FixedUpdate() {
        // TODO(bp): this may not be necessary now
        if (recorder != null) {
            recorder.Record();
        }
    }

    public override void OnExit() {
        base.OnExit();

        RecordedInput recording = recorder.Eject();

        // Prevent short recordings from getting saved
        float minNumberOfInputs =
            (float) (minRecordingThreshold / Time.fixedDeltaTime);

        if (recording.Length > minNumberOfInputs) {
            recordings.Add(recording);
        }

        recorder = null;

        StopCoroutine("SpawnGhosts");
        destroyGhosts();
        destroyEnemies();
    }

    public void OnDestroy() {
        destroyGhosts();
        destroyEnemies();
    }

    private void destroyGhosts() {
        foreach (GameObject ghost in ghosts) {
            Destroy(ghost);
        }

        ghosts.Clear();
    }

    private void destroyEnemies() {
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
            Destroy(enemy);
        }
    }
}
