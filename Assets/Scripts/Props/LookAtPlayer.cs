﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour {

    private Transform playerPos;
    public Sprite[] sprites;

    void Awake() {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        playerPos = playerObject.transform;
    }

    // Update is called once per frame
    void Update () {
        Vector3 direction = playerPos.position - transform.position;
        float x = direction[0];
        float y = direction[1];
        SpriteRenderer image = gameObject.GetComponent<SpriteRenderer>();
        Sprite sprite;
        if (Mathf.Abs(x) > Mathf.Abs(y)) {
            if (x < 0) {
                sprite = sprites[0];
            } else {
                sprite = sprites[1];
            }
        } else {
            if (y < 0) {
                sprite = sprites[3];
            } else {
                sprite = sprites[2];
            }
        }
        image.sprite = sprite;
    }
}
