﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLoader : MonoBehaviour {

    /// <summary>
    /// The game object to load overtop of this one.
    /// </summary>
    public GameObject enemy;

    // Loads in the enemy and then kills itself
    void Awake() {
        gameObject.tag = "EnemyLoader";

        // Enemy loader persists, so we must hide it. But we want it visible
        // in the editor, so we can see where the enemies are.
        GetComponent<SpriteRenderer>().enabled = false;
    }

    public void LoadEnemy() {
        GameObject load = (GameObject) Instantiate(enemy);
        load.transform.position = transform.position;
        load.transform.parent = gameObject.transform.parent;
    }
}
