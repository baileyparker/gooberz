using UnityEngine;
using UnityEngine.Assertions;


public class PlayerInput {
    public readonly Vector2 velocity;
    public readonly bool shoot;
    public readonly bool shootHold;
    public readonly bool fireUp;
    public readonly bool fireLeft;
    public readonly bool fireDown;
    public readonly bool fireRight;
    public readonly bool cycleWeapon;
    private bool ammoDepleted;

    public bool AmmoDepleted {
        get { return ammoDepleted; }
        set {
            Assert.IsTrue(value, "can only set AmmoDepleted to true");
            ammoDepleted = value;
        }
    }

    public PlayerInput(Vector2 velocity, bool[] shootDir, bool cycleWeapon) {
        this.velocity = velocity;
        // TODO(bp): Remove unneeded things & clean
        this.shoot = (shootDir[0]||shootDir[2]||shootDir[4]||shootDir[6]);
        this.shootHold = (shootDir[1]||shootDir[3]||shootDir[5]||shootDir[7]);
        this.fireUp = shootDir[0] || shootDir[1];
        this.fireLeft = shootDir[2] || shootDir[3];
        this.fireDown = shootDir[4] || shootDir[5];
        this.fireRight = shootDir[6] || shootDir[7];
        this.cycleWeapon = cycleWeapon;
        this.ammoDepleted = false;
    }

    public override bool Equals(object o) {
        if (!(o is PlayerInput)) {
            return false;
        }

        return this == ((PlayerInput) o);
    }

    public static bool operator==(PlayerInput i1, PlayerInput i2) {
        if (object.ReferenceEquals(i1, null) || object.ReferenceEquals(i2, null)) {
            return object.ReferenceEquals(i1, null) && object.ReferenceEquals(i2, null);
        }

        return i1.velocity == i2.velocity && i1.shoot == i2.shoot
            && i1.shootHold == i2.shootHold && i1.fireUp == i2.fireUp
            && i1.fireLeft == i2.fireLeft && i1.fireDown == i2.fireDown
            && i1.fireRight == i2.fireRight && i1.cycleWeapon && i2.cycleWeapon
            && i1.AmmoDepleted && i2.AmmoDepleted;
    }

    public static bool operator!=(PlayerInput i1, PlayerInput i2) {
        return !(i1 == i2);
    }

    public override int GetHashCode() {
        int hash = 13;
        hash = (hash * 7) + velocity.GetHashCode();
        hash = (hash * 7) + shoot.GetHashCode();
        hash = (hash * 7) + shootHold.GetHashCode();
        hash = (hash * 7) + fireUp.GetHashCode();
        hash = (hash * 7) + fireLeft.GetHashCode();
        hash = (hash * 7) + fireDown.GetHashCode();
        hash = (hash * 7) + fireRight.GetHashCode();
        hash = (hash * 7) + cycleWeapon.GetHashCode();
        hash = (hash * 7) + ammoDepleted.GetHashCode();
        return hash;
    }
}
