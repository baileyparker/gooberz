﻿using UnityEngine;


public class Projectile : MonoBehaviour {
    /// <summary>
    /// The amount of damage done when the projectile collides with a game
    /// object.
    /// </summary>
    public int damage = 1;

    protected virtual void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "Enemy") {
            BaseEnemy enemy = coll.gameObject.GetComponent<BaseEnemy>();
            enemy.LoseHP(damage);
            HitEnemy(enemy.hp);
        }

        if (coll.gameObject.tag != "Player") {
            Destroy(gameObject);
        }
    }

    protected virtual void HitEnemy(int hp) {
        GameManager.Instance.statsManager.UpdateEnemyHit(hp);
    }
}
