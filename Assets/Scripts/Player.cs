using UnityEngine;
using System.Collections.Generic;

public class Player : BasePlayer {

    public override void Start() {
        base.Start();

        SoundManager.Instance.AttachSound(gameObject, SoundEnum.walking, true);
        GameManager.Instance.hud.SetPlayerAmmo(ammo, currentWeapon);
    }

    protected AudioSource walkingSound {
        get { return gameObject.GetComponent<AudioSource>(); }
    }

    protected override void OnStartWalking() {
        walkingSound.UnPause();
    }

    protected override void OnStopWalking() {
        walkingSound.Pause();
    }

    protected override void OnEnterDoor(string tag) {
        GameManager.Instance.levelManager.EnterDoor(tag);
    }

    protected override void OnCollectGun(GameObject gun) {
        SpriteRenderer renderer = gun.GetComponent<SpriteRenderer>();

        // Ghosts must be able to pickup these. The easiest way to allow this
        // is to hide the pickup from rendering once the actual player has
        // picked it up (but it is still able to be collided with, so ghosts
        // that later visit it can pick it up). Placing it in the Player also
        // prevents a ghost from "stealing" the ammo pickup.
        if (renderer.enabled) {
            base.OnCollectGun(gun);
            renderer.enabled = false;
        }
    }

    protected override IPlayerInputSource GetInputSource() {
        return new KeyboardInput();
    }

    protected override GameObject shootBasic(PlayerInput input) {
        base.shootBasic(input);

        if (decAmmo) {
            decAmmo = false;
            GameManager.Instance.statsManager.IncrementShotsFired();
        }

        return null;
    }

    protected override List<GameObject> shootShotgun(PlayerInput input) {
        base.shootShotgun(input);

        if (decAmmo) {
            decAmmo = false;
            GameManager.Instance.statsManager.IncrementShotsFired();
        }

        return null;
    }

    protected override GameObject shootUzi(PlayerInput input) {
        base.shootUzi(input);

        if (decAmmo) {
            decAmmo = false;
            GameManager.Instance.statsManager.IncrementShotsFired();
        }

        return null;
    }

    protected override GameObject shootLaser(PlayerInput input) {
        base.shootLaser(input);

        if (decAmmo) {
            decAmmo = false;
            GameManager.Instance.statsManager.IncrementShotsFired();
        }

        return null;
    }

    public override void CheckGameover() {
        base.CheckGameover();

        if (hp <= 0) {
            OnStopWalking();
        }
    }
}
