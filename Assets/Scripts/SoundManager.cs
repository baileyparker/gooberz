﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SoundEnum {
    none,

    //Sound Tracks
    mars,
    fanfare,
    bolero,
    symphony,
    nocturne,

    //Sound Effects
    explosion1,
    whoosh1,
    walking,
    transition01Explosion,
}

//potentially clean up process with:
// https://answers.unity.com/questions/340859/create-audiosource-via-script.html
public class SoundManager : MonoBehaviour {

    private static SoundManager _instance = null;

    public static SoundManager Instance {
        get { return SoundManager._instance; }
    }

    
    private Dictionary<SoundEnum, AudioClip> soundTracks = new Dictionary<SoundEnum, AudioClip>();
    private Dictionary<SoundEnum, AudioClip> soundEffects = new Dictionary<SoundEnum, AudioClip>();
    
    public AudioClip track_mars;
    public AudioClip track_fanfare;
    public AudioClip track_bolero;
    public AudioClip track_symphony;
    public AudioClip track_nocturne;

    public AudioClip effect_explosion1;
    public AudioClip effect_whoosh1;
    public AudioClip effect_walking;
    public AudioClip effect_01_transition_explosion;


    //hold all audiosources for volume control
    private List<AudioSource> attachedTracks = new List<AudioSource>();
    private List<AudioSource> attachedEffects = new List<AudioSource>();

    private float musicVolume = 1;
    private float effectVolume = 1;

    public void Awake() {
        if (Instance == null) {
            _instance = this;

            //insert all sound tracks into the track set
            soundTracks.Add(SoundEnum.bolero, track_bolero);
            soundTracks.Add(SoundEnum.fanfare ,track_fanfare);
            soundTracks.Add(SoundEnum.mars, track_mars);
            soundTracks.Add(SoundEnum.nocturne, track_nocturne);
            soundTracks.Add(SoundEnum.symphony, track_symphony);

            //insert all sound effects into the effect set
            soundEffects.Add(SoundEnum.explosion1, effect_explosion1);
            soundEffects.Add(SoundEnum.walking, effect_walking);
            soundEffects.Add(SoundEnum.whoosh1, effect_whoosh1);
            soundEffects.Add(SoundEnum.transition01Explosion, effect_01_transition_explosion);
        }
    }

    public void AttachSound(GameObject obj, SoundEnum request, bool loop=false) {
        if (request != SoundEnum.none) {
            //create a new sound object attached to the specified game object
            AudioSource track = obj.AddComponent<AudioSource>();

            //add the actual sound
            if (soundEffects.ContainsKey(request)) {
                //set the track to play the sound effect
                track.clip = soundEffects[request];
                track.volume = effectVolume;

                //add the sound object to the list of sound effects (for volume control)
                attachedEffects.Add(track);
            } else if (soundTracks.ContainsKey(request)) {
                //set the track to play the sound track
                track.clip = soundTracks[request];
                track.volume = musicVolume;

                //add to the list of sound tracks for global volume control
                attachedTracks.Add(track);
            } else {
                Debug.LogError("Sound requested is neither a soundtrack nor an effect. Requested " + request.ToString());
            }

            //start the sound (loop if specified, and set the volume to current)
            track.loop = loop;
            track.Play();
        }
    }

    public void StartSoundTrack(SoundEnum request) {
        if (request != SoundEnum.none) {
            //fade out any existing soundtracks
            if (Camera.main.GetComponent<AudioSource>() != null) {
                StopSoundTrack();
            }

            //soundtracks are attached to the main camera
            AttachSound(Camera.main.gameObject, request, true);
        }
    }

    public void StopSoundTrack(float FadeTime=5.0f) {
        if (Camera.main.gameObject.GetComponent<AudioSource>() != null) {
            IEnumerator fadeout = FadeOutSound(Camera.main.gameObject.GetComponent<AudioSource>(), FadeTime);
            StartCoroutine(fadeout);
        }
    }

    //source: https://forum.unity.com/threads/fade-out-audio-source.335031/
    public static IEnumerator FadeOutSound(AudioSource audioSource, float FadeTime) {
        float startVolume = audioSource.volume;

        while (audioSource != null && audioSource.volume > 0) {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return null;
        }

        if (audioSource != null) {
            Destroy(audioSource);
            yield break;
        }
    }

    public void SetSoundTrackVolume(float volume) {
        musicVolume = volume;
        foreach (AudioSource i in attachedTracks) {
            i.volume = musicVolume;
        }
    }

    public void SetSoundEffectVolume(float volume) {
        effectVolume = volume;
        foreach (AudioSource i in attachedEffects) {
            i.volume = effectVolume;
        }
    }

    public void SetGlobalVolume(float volume) {
        SetSoundEffectVolume(volume);
        SetSoundTrackVolume(volume);
    }
}
