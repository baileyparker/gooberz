﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue {

	public DialogueSpeaker speaker;
	public string text;
	public int duration;

}
