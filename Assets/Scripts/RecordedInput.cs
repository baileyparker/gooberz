using System;
using System.Collections.ObjectModel;

using UnityEngine;
using UnityEngine.Assertions;


public class RecordedInput : IPlayerInputSource {
    public readonly Vector3 StartingPosition;
    private ReadOnlyCollection<InputEvents> inputs;

    private int index = 0;
    private int repeats = 0;

    private Action onFinish;


    public RecordedInput(Vector3 startingPosition,
                         ReadOnlyCollection<InputEvents> inputs) {
        this.StartingPosition = startingPosition;
        this.inputs = inputs;
    }

    public PlayerInput GetInput() {
        if (index >= inputs.Count) {
            if (onFinish != null) {
                onFinish();
            }

            // TODO: fallthrough for now (revisit later)
            // return null;
            Assert.IsTrue(index == 0 && repeats == 0, "recording was not rewound");
        }

        InputEvents e = inputs[index];

        if (++repeats > e.repeats) {
            index++;
            repeats = 0;
        }

        return e.input;
    }

    public void Rewind() {
        index = 0;
        repeats = 0;
    }

    public int Length {
        get {
            int length = 0;

            foreach (InputEvents e in inputs) {
                length += 1 + e.repeats;
            }

            return length;
        }
    }

    public void OnFinish(Action onFinish) {
        this.onFinish = onFinish;
    }
}
