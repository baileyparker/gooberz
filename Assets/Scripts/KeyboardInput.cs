using UnityEngine;


public class KeyboardInput : IPlayerInputSource {
    public PlayerInput GetInput() {
        Vector2 velocity = new Vector2(Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical"));

        bool fireUp = Input.GetButtonDown("FireUp");
        bool fireUpHold = Input.GetButton("FireUp");
        bool fireLeft = Input.GetButtonDown("FireLeft");
        bool fireLeftHold = Input.GetButton("FireLeft");
        bool fireDown = Input.GetButtonDown("FireDown");
        bool fireDownHold = Input.GetButton("FireDown");
        bool fireRight = Input.GetButtonDown("FireRight");
        bool fireRightHold = Input.GetButton("FireRight");

        bool[] shootDir = new bool[] {fireUp, fireUpHold, fireLeft, fireLeftHold,
            fireDown, fireDownHold, fireRight, fireRightHold};

        bool cycleWeapon = Input.GetButtonDown ("Fire3");

        return new PlayerInput(velocity, shootDir, cycleWeapon);
    }
}
