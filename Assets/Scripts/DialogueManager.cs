using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Assertions;


public class DialogueManager : MonoBehaviour {
    protected GameObject textObject;
    protected Coroutine hideDialogue;
    protected Coroutine typingText;

    public virtual void Awake() {
        textObject = GameObject.FindWithTag("DialogueText");
        Assert.IsNotNull(textObject, "couldn't find text");
    }

    protected string text {
        set {
            GetComponent<Text>().text = value;
        }
    }

    public virtual void Display(DialogueSpeaker speaker, string dialogue,
                        float duration) {
        gameObject.SetActive(true);
        if (typingText != null) {
            StopCoroutine(typingText);
        }
        typingText = StartCoroutine(TypingText(dialogue));

        if (hideDialogue != null) {
            StopCoroutine(hideDialogue);
        }

        hideDialogue = StartCoroutine(hide(duration));
    }

    IEnumerator TypingText(string sentence) {
        textObject.GetComponent<Text>().text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            textObject.GetComponent<Text>().text += letter;
            yield return null;
        }
    }

    protected virtual IEnumerator hide(float delay) {
        yield return new WaitForSeconds(delay);

        gameObject.SetActive(false);
        hideDialogue = null;
    }
}
