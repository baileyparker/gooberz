﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WavingFlag : MonoBehaviour {
    public GameObject FlagObject;
    public GameObject FlagMaskObject;

    public int numDisivions = 10;

    [Range(0.0f, 10.0f)]
    public float speed = 1;

    [Range(0.0f, 10.0f)]
    public float amplitude = 1;

    [Range(0.0f, 2*Mathf.PI)]
    public float phase = 0;

    [Range(0.0f, 3.0f)]
    public float frequency = 1; //how many sin waves over flag length

    List<GameObject> flagSections;

    // Use this for initialization
    void Start() {
        flagSections = new List<GameObject>();

        // Break flag sprite into numDisivions sections
        Vector3 flag_size = FlagObject.GetComponent<Renderer>().bounds.size;
        float width = flag_size.x;
        float height = flag_size.y;

        float x0 = transform.position.x;
        float y0 = transform.position.y;
        float z0 = transform.position.z;

        float div_width = width / numDisivions;

        for (int i = 0; i < numDisivions; i++) {
            GameObject maskObj = Instantiate(FlagMaskObject, this.transform);

            SpriteMask m = maskObj.GetComponent<SpriteMask>();
            GameObject f = Instantiate(FlagObject, transform);

            Vector3 mOldSize = maskObj.GetComponent<Renderer>().bounds.size;

            float mNewWidth = maskObj.transform.localScale.x * div_width / mOldSize.x / 2;
            // Double width to allow for waving to fit without having to move the mask
            float mNewHeight = maskObj.transform.localScale.y * height / mOldSize.y;
            float mNewDepth = maskObj.transform.localScale.z;

            maskObj.transform.localScale = new Vector3(mNewWidth, mNewHeight, mNewDepth);
            maskObj.transform.position = new Vector3(x0 - width / 2 + div_width * (i + 0.5f), y0, z0);

            f.transform.position = gameObject.transform.position;

            // Set the cutoff range the mask acts on
            m.frontSortingOrder = i;
            m.backSortingOrder = i - 1;
            m.isCustomRangeActive = true;

            f.GetComponent<SpriteRenderer>().sortingOrder = i;
            f.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;

            flagSections.Add(f);
        }
    }

    // Update is called once per frame
    void Update () {
        int i = 0;

        float ySetpoint = amplitude * Mathf.Sin(((Time.time) * speed) + phase);

        foreach (var f in flagSections) {
            Vector3 wavPos = transform.position;
            wavPos.y = - ySetpoint + wavPos.y + amplitude * Mathf.Sin(((Time.time) * speed) + Mathf.Lerp(0.0f, 2 * Mathf.PI * frequency, i / (float)numDisivions) + phase);
            f.transform.position = wavPos;

            i++;
        }
    }
}
