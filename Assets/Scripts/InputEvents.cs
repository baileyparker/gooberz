public class InputEvents {
    public readonly PlayerInput input;
    public readonly int repeats;

    public InputEvents(PlayerInput input, int repeats) {
        this.input = input;
        this.repeats = repeats;
    }
}
