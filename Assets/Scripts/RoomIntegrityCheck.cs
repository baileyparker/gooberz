using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;


public class RoomIntegrityCheck {
    public static void AssertRoomsProperlyConfigured() {
        Dictionary<RoomName, Room> rooms = TryLoadingRooms();

        foreach (KeyValuePair<RoomName, Room> kvp in rooms) {
            AssertGoodLinks(rooms, kvp.Key, kvp.Value);
        }

        foreach (Room room in rooms.Values) {
            UnityEngine.Object.Destroy(room.gameObject);
        }
    }

    private static Dictionary<RoomName, Room> TryLoadingRooms() {
        Dictionary<RoomName, Room> rooms = new Dictionary<RoomName, Room>();

        foreach (RoomName name in Enum.GetValues(typeof(RoomName))) {
            if (name != RoomName.Nowhere) {
                rooms[name] = TryLoadingRoom(name);
            }
        }

        return rooms;
    }

    private static Room TryLoadingRoom(RoomName name) {
        string resourceName = name.ResourceName();
        GameObject resource =
            Resources.Load(resourceName, typeof(GameObject)) as GameObject;

        string path = "Prefabs/Resources/" + resourceName + ".prefab";
        Assert.IsNotNull(resource,
                         "unable to load prefab for " + name + ", " +
                         "it should be in: " + path);

        GameObject gameObject = UnityEngine.Object.Instantiate(resource);
        Assert.IsNotNull(gameObject,
                         string.Format("unable to instantiate room: {0} ({1})",
                                       name, resourceName));

        Room room = gameObject.GetComponent<Room>();
        Assert.IsNotNull(room,
                         string.Format("room doesn't have Room script: {0} ({1})",
                                       name, resourceName));

        gameObject.SetActive(false);
        return room;
    }

    private static void AssertGoodLinks(Dictionary<RoomName, Room> rooms,
                                          RoomName name, Room room) {
        foreach (RoomDirection direction in Enum.GetValues(typeof(RoomDirection))) {
            AssertGoodLink(rooms, name, room, direction);
        }

        AssertNoDoubleLinks(name, room);
    }

    private static void AssertGoodLink(Dictionary<RoomName, Room> rooms,
                                         RoomName name, Room room,
                                         RoomDirection direction) {
        RoomName linkedTo = direction.GetLink(room);

        if (linkedTo != RoomName.Nowhere) {
            string humanName = "room " + name + " " + direction + " door";

            // Ensure no self links
            Assert.IsFalse(name == linkedTo, humanName + " is linked to itself");

            // Ensure every link has an opposite link coming back
            Room linkedToRoom = rooms[linkedTo];
            RoomDirection opposite = direction.Opposite();
            RoomName oppositeLink = opposite.GetLink(linkedToRoom);
            string oppositeLinkName =
                "room " + linkedTo + " " + opposite + " door";
            Assert.IsTrue(oppositeLink == name,
                          humanName + " is linked to " + linkedTo +
                          " but " + oppositeLinkName + " is not linked to " + name +
                          " (instead linked to " + oppositeLink + ")");
        }
    }

    private static void AssertNoDoubleLinks(RoomName name, Room room) {
        foreach (RoomDirection direction in Enum.GetValues(typeof(RoomDirection))) {
            foreach (RoomDirection otherDirection in Enum.GetValues(typeof(RoomDirection))) {
                if (otherDirection > direction) {
                    RoomName link = direction.GetLink(room);
                    RoomName otherLink = otherDirection.GetLink(room);

                    if (direction != otherDirection && link != RoomName.Nowhere) {
                        string door = direction + " door";
                        string otherDoor = otherDirection + " door";
                        string doors = name + " " + door + " and " + otherDoor;

                        Assert.IsTrue(link != otherLink,
                                      doors + " link to same place " + link);
                    }
                }
            }
        }
    }
}

enum RoomDirection {
    East, West, North, South
}

static class RoomDirectionExtensions {
    public static RoomDirection Opposite(this RoomDirection direction) {
        switch (direction) {
            case RoomDirection.East:
                return RoomDirection.West;
            case RoomDirection.West:
                return RoomDirection.East;
            case RoomDirection.North:
                return RoomDirection.South;
            case RoomDirection.South:
                return RoomDirection.North;
            default:
                Debug.LogError(String.Format("Illegal enum value {0}", direction));
                return (RoomDirection)(-1);
        }
    }

    public static RoomName GetLink(this RoomDirection direction, Room room) {
        switch (direction) {
            case RoomDirection.East:
                return room.eastRoomName;
            case RoomDirection.West:
                return room.westRoomName;
            case RoomDirection.North:
                return room.northRoomName;
            case RoomDirection.South:
                return room.southRoomName;
            default:
                Debug.LogError(String.Format("Illegal enum value {0}", direction));
                return (RoomName)(-1);
        }
    }
}
