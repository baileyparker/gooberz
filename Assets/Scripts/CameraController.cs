﻿using UnityEngine;


public class CameraController : MonoBehaviour {
    /// <summary>
    /// The player around which the camera centers itself (within the window
    /// described below).
    /// </summary>
    public Player player;

    /// <summary>
    /// A window centered about the camera's position in which the player can move
    /// without panning the camera. Once the player moves beyond this window, the
    /// camera begins to pan to compensate;
    /// </summary>
    public readonly Vector3 movementWindow = new Vector3(3f, 2f, 0f);


    void LateUpdate() {
        Vector3 cameraOffset = player.transform.position - transform.position;

        // Absolute offset of player from window (maxed with zero to avoid moving
        // when the player is inside the window)
        Vector3 windowOffset = Vector3.Max(cameraOffset.Abs() - (movementWindow / 2),
                                           Vector3.zero);

        // Move the camera by the magnitude of windowOffset and direction of
        // cameraOffset (so that the player is again within the window)
        Vector3 delta = Vector3.Scale(windowOffset, cameraOffset.UnitComponents()).NoZ();
        transform.position += delta;
    }
}
