﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    public int ammo;
    public enum Weapon {Basic, Uzi, Shotgun, Laser}
    public Weapon weapon;
    public static Sprite[] sprites = new Sprite[4];

    public void Awake() {
        SetImage(weapon);
    }

    private void SetImage(Gun.Weapon weapon) {
        SpriteRenderer image = gameObject.GetComponent<SpriteRenderer>();
        Sprite sprite;
        switch (weapon) {
            case Gun.Weapon.Basic:
                sprite = sprites[0];
                break;
            case Gun.Weapon.Uzi:
                sprite = sprites[1];
                break;
            case Gun.Weapon.Shotgun:
                sprite = sprites[2];
                break;
            case Gun.Weapon.Laser:
                sprite = sprites[3];
                break;
            default:
                sprite = sprites[0];
                break;
        }
        image.sprite = sprite;
    }

}
