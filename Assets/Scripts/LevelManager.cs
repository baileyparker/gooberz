using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;


public class LevelManager : MonoBehaviour {
    /// <summary>
    /// Transition style to use for room-to-room transition animations.
    /// </summary>
    public TransitionStyle TransitionStyle = TransitionStyle.BindingSwipe;

    /// <summary>
    /// Duration of room-to-room transition animations in seconds.
    /// </summary>
    [Range(0.1f, 5.0f)]
    public float TransitionDuration = 0.25f;

    /// <summary>
    /// The room in which the player currently resides.
    /// </summary>
    private GameObject currentRoom;

    /// <summary>
    /// The name of the room in which the player currently resides.
    /// </summary>
    private RoomName currentRoomName;

    /// <summary>
    /// When transitioning, the current transition object (responsible for
    /// animating the transition).
    /// </summary>
    private Transition currentTransition;

    /// <summary>
    /// All of the rooms that have been instantiated to this point.
    /// </summary>
    private Dictionary<RoomName, GameObject> rooms =
        new Dictionary<RoomName, GameObject>();

    /// <summary>
    /// The object used by transitions to add overlays, etc.
    /// </summary>
    private GameObject transitionObject;


    void Awake() {
        transitionObject =
            (GameObject) Instantiate(Resources.Load("TransitionObject"));
        Assert.IsNotNull(transitionObject,
                         "couldn't instantiate TransitionObject");

        if (Application.isEditor) {
            RoomIntegrityCheck.AssertRoomsProperlyConfigured();
        }

        // Load first room of first level
        ImmediatelyLoadRoom(RoomName.Level0Room1, null);
    }

    void Update() {
        if (currentTransition != null) {
            if (!currentTransition.Continue()) {
                // Clear once the transition finishes
                currentTransition = null;
            }
        }
    }

    public void EnterDoor(string doorTag) {
        Assert.IsNotNull(currentRoom, "LevelManager has no current room");
        Room roomScript = currentRoom.GetComponent<Room>();

        if (doorTag.Contains("Locked")) {
            if (GameManager.Instance.Player.hasKey) {
                doorTag = doorTag.Replace("Locked", "");
            } else {
                GameManager.Instance.dialogueManager.Display(DialogueSpeaker.Janitor, "It seems this door needs a key", 5);
                return;
            }
        }
        // TODO(bp): enum on door may be better!
        switch (doorTag) {
            case "EastDoor":
                TransitionToRoom(roomScript.eastRoomName, "WestDoor");
                break;
            case "WestDoor":
                TransitionToRoom(roomScript.westRoomName, "EastDoor");
                break;
            case "NorthDoor":
                TransitionToRoom(roomScript.northRoomName, "SouthDoor");
                break;
            case "SouthDoor":
                TransitionToRoom(roomScript.southRoomName, "NorthDoor");
                break;
            default:
                throw new System.Exception(string.Format("bad room tag: {0}",
                                                   doorTag));
        }
    }

    public void TransitionToRoom(RoomName room, string entryDoor) {
        StartTransition(TransitionStyle, TransitionDuration, room, entryDoor);
    }

    // Retains original functionality of swapping directly to the specified room
    // without transition
    public void ImmediatelyLoadRoom(RoomName room, string entryDoor) {
        StartTransition(TransitionStyle.None, 0f, room, entryDoor);
    }

    protected void StartTransition(TransitionStyle style, float duration,
                                   RoomName room, string entryDoor) {
        if (currentTransition == null) {
            Debug.Log("Transition to " + room + " from " + entryDoor + " by " + style);

            GameObject previousRoom = currentRoom;
            currentRoom = LoadRoom(room);
            currentRoomName = room;

            currentTransition = style.Instantiate(duration, previousRoom,
                                                  currentRoom, entryDoor,
                                                  transitionObject);
        }
    }

    protected GameObject LoadRoom(RoomName roomName) {
        if (!rooms.ContainsKey(roomName)) {
            // Or load room prefab (if never loaded before)
            string resourceName = roomName.ResourceName();
            GameObject resource =
                Resources.Load(resourceName, typeof(GameObject)) as GameObject;
            Assert.IsNotNull(resource,
                             string.Format("unable to load room: {0} ({1})",
                                           roomName, resourceName));

            rooms[roomName] = Instantiate(resource);
            Assert.IsNotNull(rooms[roomName],
                             string.Format("unable to instantiate room: {0} ({1})",
                                           roomName, resourceName));
        }

        GameObject room = rooms[roomName];
        room.SetActive(true);
        return room;
    }

    public void Reset() {
        foreach (GameObject room in rooms.Values) {
            Destroy(room);
        }

        rooms.Clear();
    }

    public void ReloadCurrentLevel() {
        RoomName level = currentRoomName.LevelStart();
        Reset();
        ImmediatelyLoadRoom(level, null);
    }

    public void GameOver() {
        currentRoom.SetActive(false);
    }
}
