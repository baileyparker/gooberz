﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class CutSceneManager : DialogueManager {
    private GameObject presidentTextObject;
    private GameObject playerTextObject;
    private bool doneTyping;
    public List<Dialogue> dialogues;
    private string currSentence;

    public override void Awake() {
        base.Awake();
        presidentTextObject = GameObject.FindWithTag("PresidentTextBox");
        playerTextObject = GameObject.FindWithTag("PlayerTextBox");
        presidentTextObject.SetActive(false);
        playerTextObject.SetActive(false);
        StartDialogue();
    }

    public void Update() {
    	if (Input.GetKeyDown("space")) {
    		if (!doneTyping) {
    			if (typingText != null) {
            		StopCoroutine(typingText);
        		}
        		doneTyping = true;
        		textObject.GetComponent<Text>().text = currSentence;
    		} else {
    			DisplayNextSentence();
    		}
    	}
    }

    private void StartDialogue() {
    	StartCoroutine(wait(2));
    }

    private void DisplayNextSentence() {
    	if (dialogues.Count > 0) {
    		Dialogue d = dialogues[0];
    		dialogues.RemoveAt(0);
    		Display(d.speaker, d.text, d.duration);
    		doneTyping = false;
    	} else {
    		SceneManager.LoadScene("MainScene");
    	}
    }

    public override void Display(DialogueSpeaker speaker, string dialogue,
                        float duration) {
        ShowProperTextBox(speaker);
        currSentence = dialogue;
        if (typingText != null) {
            StopCoroutine(typingText);
        }
        typingText = StartCoroutine(TypingText(dialogue));

        if (hideDialogue != null) {
            StopCoroutine(hideDialogue);
        }

        hideDialogue = StartCoroutine(hide(duration));
    }

    IEnumerator TypingText(string sentence) {
        textObject.GetComponent<Text>().text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            textObject.GetComponent<Text>().text += letter;
            yield return null;
        }
        doneTyping = true;
    }

    private void ShowProperTextBox(DialogueSpeaker speaker) {
    	if (speaker == DialogueSpeaker.Janitor) {
    		presidentTextObject.SetActive(false);
    		playerTextObject.SetActive(true);
    	} else if (speaker == DialogueSpeaker.President) {
    		playerTextObject.SetActive(false);
    		presidentTextObject.SetActive(true);
    	}
    }

    protected override IEnumerator hide(float delay) {
        yield return new WaitForSeconds(delay);

        presidentTextObject.SetActive(false);
    	playerTextObject.SetActive(false);
    	textObject.GetComponent<Text>().text = "";
        hideDialogue = null;
    }

    private IEnumerator wait(float delay) {
    	yield return new WaitForSeconds(delay);
    	DisplayNextSentence();
    }
}
