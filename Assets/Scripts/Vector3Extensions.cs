using UnityEngine;


public static class Vector3Extensions {
    public static Vector3 Abs(this Vector3 vec) {
        return new Vector3(Mathf.Abs(vec.x), Mathf.Abs(vec.y), Mathf.Abs(vec.z));
    }

    public static Vector3 UnitComponents(this Vector3 vec) {
        return new Vector3(Mathf.Sign(vec.x), Mathf.Sign(vec.y), Mathf.Sign(vec.z));
    }

    public static Vector3 NoZ(this Vector3 vec) {
        return new Vector3(vec.x, vec.y, 0f);
    }
}
