using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour {
    protected const int STARTING_SCORE = 100000;
    protected const int SECOND_DECREMENT = 10;

    protected Text scoreText;
    protected int _score;
    protected int score {
        get { return _score; }
        set {
            if (value >= 0) {
                _score = value;
            } else {
                _score = 0;
            }

            scoreText.text = _score.ToString();
        }
    }

    public void Awake() {
        GameObject scoreObject = GameObject.FindWithTag("HUDScore");
        Assert.IsNotNull(scoreObject, "couldn't find score object");

        scoreText = scoreObject.GetComponent<Text>();
        Assert.IsNotNull(scoreText, "couldn't find text attached to score object");

    }

    public void Start() {
        score = STARTING_SCORE;
        StartCoroutine("TickScore");
    }

    protected IEnumerator TickScore() {
        while (true) {
            yield return new WaitForSeconds(.5f);
            score -= SECOND_DECREMENT;
        }
    }

    public void AddScore(int delta) {
        score += delta;
    }

    public void Reset() {
        score = STARTING_SCORE;
    }
}
