﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableWall : MonoBehaviour {

	public GameObject HideSecretOverlay;

    public void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.name == "Projectile(Clone)") {
            Disappear();
        }
    }

    protected virtual void Disappear() {
    	gameObject.SetActive(false);
    	HideSecretOverlay.SetActive(false);
    }
}
