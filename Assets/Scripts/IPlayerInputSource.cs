public interface IPlayerInputSource {
    PlayerInput GetInput();
}
