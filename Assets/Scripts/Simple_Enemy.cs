﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simple_Enemy : MonoBehaviour {
    public float maxVelocity = 1f;
    public int damage = 1;
    public int health = 2;


    void Update() {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        Vector3 player = playerObject.transform.position;
        Vector3 delta = Vector3.ClampMagnitude(player - transform.position,
                                               maxVelocity);
        GetComponent<Rigidbody2D>().velocity = delta;
    }

    protected void OnTriggerEnter2D(Collider2D other) {
        Player player = other.gameObject.GetComponent<Player>();

        if (player != null && player.LoseHP(damage) == 0) {
            GetComponent<Animator>().SetTrigger("EnemyAttack");
        }
    }

    public void LoseHealth(int loss) {
        health -= loss;
        if (health <= 0) {
            Destroy(gameObject);
        }
    }
}
