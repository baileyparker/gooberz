using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public class InputRecorder {
    private IPlayerInputSource source;
    private Vector3 startingPosition;
    private List<InputEvents> inputs = new List<InputEvents>();
    private PlayerInput lastInput;
    private int repeats = 0;

    public InputRecorder(IPlayerInputSource source, Vector3 startingPosition) {
        this.source = source;
        this.startingPosition = startingPosition;
    }

    public void Record() {
        PlayerInput newInput = source.GetInput();

        if (newInput == lastInput) {
            repeats++;
        } else {
            if (lastInput != null) {
                Push();
            }

            lastInput = newInput;
            repeats = 0;
        }
    }

    private void Push() {
        inputs.Add(new InputEvents(lastInput, repeats));
        repeats = 0;
    }

    public RecordedInput Eject() {
        return new RecordedInput(this.startingPosition, inputs.AsReadOnly());
    }
}
