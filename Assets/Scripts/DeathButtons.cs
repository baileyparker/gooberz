﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathButtons : MonoBehaviour {

    public void Menu() {
        SceneManager.LoadScene("MainMenu");
    }

    public void Retry() {
        GameManager.Instance.hud.HideDeathScreen();
        GameManager.Instance.statsManager.Reset();
        GameManager.Instance.levelManager.ReloadCurrentLevel();
        GameManager.Instance.Player.Reset();
    }
}
