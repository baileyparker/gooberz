﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelZeroToOneTransitionNarrative : MonoBehaviour {
    Camera cam;
    public GameObject explosion_prefab;
    ArrayList explosions = new ArrayList();
    GameObject player;
    public GameObject hole;
    public float maxShakeAngle;
    public float maxShakeOffset;

    float ortho_init;

    Vector3 cam_origin;
    float shake = 0;
    public int count = 100;
    int key_one = 90;
    int key_two = -170;
    int key_end = -370;
    int shake_length = 200;

    public bool playTransition = false;

    private void OnEnable() {
        transform.position = GameObject.Find("Room 6(Clone)").transform.position;

        cam = Camera.main;
        ortho_init = cam.orthographicSize;
        cam_origin = cam.transform.position;

        transform.position = new Vector3(64, -1, 10);

        hole = Instantiate(hole, transform, false);
        hole.SetActive(false);
        player = GameObject.Find("Player");
    }

    private void FixedUpdate() {
        if (playTransition) {
            // Shake camera by value in shake
            cam.transform.position = cam_origin + maxShakeOffset * shake * ((Vector3)Random.insideUnitCircle + Vector3.back * 10);
            cam.transform.rotation = Quaternion.Euler(0, 0, maxShakeAngle * shake * ((Random.value * 2) - 1));

            if (count > key_one) {
                if (Random.Range(0, 7) == 0) {
                    count--;
                    explosions.Add(Instantiate(explosion_prefab, (Vector2)transform.position + Random.insideUnitCircle * 2, Quaternion.identity));

                    // Potentially instantiate the explosion sound
                    if (Random.Range(0, 2) == 0) {
                        SoundManager.Instance.StopSoundTrack();
                        SoundManager.Instance.AttachSound(gameObject, SoundEnum.transition01Explosion);
                    }
                }
            } else if (count == key_one) {
                player.SetActive(false);
                hole.SetActive(true);
                hole.transform.position = new Vector3(64, -1, 10);
                //Debug.Log(hole.transform.position);
                //hole.transform.position = Vector3.zero;
                //Debug.Log(hole.transform.position);
                count--;
            } else if (count > key_one - shake_length) {
                float fraction = count - key_one + shake_length;
                shake = (fraction * fraction) / (shake_length * shake_length);
                count--;
            } else if (count > key_two) {
                count--;
            } else if (count > key_end) {
                // Camera zoom into the hole
                float v = (count - key_end) / (float)(key_two - key_end);
                cam.orthographicSize = ortho_init * Mathf.Pow(v, 8) + 0.05f;
                count--;
            } else if (count == key_end) {
                // Destroy all of the explosion animations
                foreach (GameObject obj in explosions) {
                    Destroy(obj);
                }

                cam.orthographicSize = ortho_init;
                cam.transform.position = cam_origin;
                player.SetActive(true);
                player.GetComponent<Player>().resumePlayerControl(); //prevent player movement during transition
                Destroy(gameObject);
                Debug.Log("here");
                GameManager.Instance.levelManager.ImmediatelyLoadRoom(RoomName.Level1Room1, "EastDoor");
                SoundManager.Instance.StartSoundTrack(SoundEnum.fanfare);
            }
        }
    }

    public void StartTransition() {
        playTransition = true;
        player.GetComponent<Player>().pausePlayerControl(); //prevent player movement during transition
    }
}
