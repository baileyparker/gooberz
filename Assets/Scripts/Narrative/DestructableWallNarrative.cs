﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DestructableWallNarrative : GenericNarrativeDialog {
    int count = 0;

    private void OnTriggerEnter2D(Collider2D collision) {
        // TODO(BP): inherit behavior from GenericNarrativeDialog
        if (collision.gameObject.name == "Player") {
            DialogueManager dialogue = GameManager.Instance.dialogueManager;
            Debug.Log(count);

            if (count++ < 2) {
                dialogue.Display(DialogueSpeaker.Janitor,
                                 "This wall Looks suspicious...", duration);
            } else {
                dialogue.Display(DialogueSpeaker.Janitor,
                                 "Maybe I should shoot it", duration);
            }

            if (nextNarrative != null) {
                nextNarrative.SetActive(true);
            }

            if (prevNarrative != null) {
                prevNarrative.SetActive(false);
            }
        }
    }
}
