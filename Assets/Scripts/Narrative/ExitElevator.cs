﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitElevator : MonoBehaviour {

   

    public GameObject LeftDoor;
    Vector3 LeftDoorInitPos;
    public GameObject RightDoor;
    Vector3 RightDoorInitPos;
    //public float maxShakeOffset = 0.025f;
    //public float maxShakeAngle = 3f;
    public GameObject wall1; //walls behind elevator need to be disabled
    public GameObject wall2;

    public GameObject player;

    bool transitionInProgress = true;
    bool firstFrame = true;

    bool talkedYet = false;
    public string exitSpeech;

    float startTime;
    Vector3 playerStartPos = new Vector3(-28.5f, -12.5f, 0f);
    Vector3 camStartPos = new Vector3(-28.5f, -13f, -10f);
    Vector3 playerFrontOfElevatorPos = new Vector3(-28.5f, 2.5f, 0f);
    Vector3 camFrontofElevatorPos = new Vector3(-28.5f, 2f, -10f);
    Vector3 playerInsideElevatorPos = new Vector3(-28.5f, 4.5f, 0f);


    //start at t=0 when the player collides with the narrative trigger
    //for each of these, force the player to be in the same spot (maybe deactivate player control script)

    float key0 = 2f;  //finish panning the camera up to the elevator
    float key1 = 2.5f;  //pause for a moment
    float key2 = 4.5f; //open the doors
    float key3 = 6.5f; //player walks out
    float key4 = 8.5f; //elevator doors close

    // Use this for initialization
    void Start () {
        startTime = Time.time;
        
        //hide walls behind the elevator doors
        wall1.SetActive(false);
        wall2.SetActive(false);

        //make the elevator doors appear in front of the player (for when they close)
        RightDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;
        LeftDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;

        //get the positions of the elevator doors
        LeftDoorInitPos = LeftDoor.transform.position;
        RightDoorInitPos = RightDoor.transform.position;

        //set the camera position to the starting position
        Camera.main.transform.position = camStartPos;

        //set the player position to the starting position
        player = GameObject.Find("Player");
        player.transform.position = playerStartPos;
        player.GetComponent<Player>().pausePlayerControl();
        player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0); //set the sprite to invisible

    }


    // Update is called once per frame
    void Update () {
        if (transitionInProgress) {
            ExitElevatorClip();
        }
    }

    void ExitElevatorClip() {
        float t = Time.time - startTime;

        //pan camera up to the elevator
        if (t > 0 && t < key0) {
            float f = t / key0; //fraction of this interval for Lerp

            Camera.main.transform.position = Vector3.Lerp(camStartPos, camFrontofElevatorPos, f);
            player.transform.position = Vector3.Lerp(playerStartPos, playerInsideElevatorPos, f);
        }

        //pause for a moment
        if (t > key0 && t < key1) {
            //initial set up (performed here so that the player doesn't accidentally clip behind the elevator doors)
            if (firstFrame) {
                //turn player color back on.
                player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);

                firstFrame = false;
            }

            //force player position
            player.transform.position = playerInsideElevatorPos;
        }

        //open the elevator doors
        if (t > key1 && t < key2) {
            float f = (t - key1) / (key2 - key1);
            player.transform.position = playerInsideElevatorPos;
            LeftDoor.transform.position = Vector3.Lerp(LeftDoorInitPos, LeftDoorInitPos + Vector3.left * 0.8f, f);
            RightDoor.transform.position = Vector3.Lerp(RightDoorInitPos, RightDoorInitPos + Vector3.right * 0.8f, f);
        }

        //player walks out
        if (t > key2 && t < key3) {
            float f = (t - key2) / (key3 - key2);
            player.transform.position = Vector3.Lerp(playerInsideElevatorPos, playerFrontOfElevatorPos, f);
            player.GetComponent<Animator>().SetFloat("xvel", 0.0f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", -0.5f);

            if (exitSpeech != null && exitSpeech.Length > 0) {
                if (!talkedYet) {
                    GameManager.Instance.dialogueManager.Display(DialogueSpeaker.Janitor, exitSpeech, 7);
                    talkedYet = true;
                }
            }
        }

        //doors close
        if (t > key3 && t < key4)  {
            float f = (t - key3) / (key4 - key3);
            LeftDoor.transform.position = Vector3.Lerp(LeftDoorInitPos + Vector3.left * 0.8f, LeftDoorInitPos, f);
            RightDoor.transform.position = Vector3.Lerp(RightDoorInitPos + Vector3.right * 0.8f, RightDoorInitPos, f);
            player.transform.position = playerFrontOfElevatorPos;
            player.GetComponent<Animator>().SetFloat("xvel", 0.0f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", 0.0f);
        }

        //clean up after completed transition
        if (t > key4) {
            //make walls collidable
            wall1.SetActive(true);
            wall2.SetActive(true);

            //set elevator door layer to proper
            LeftDoor.GetComponent<SpriteRenderer>().sortingOrder = -1;
            RightDoor.GetComponent<SpriteRenderer>().sortingOrder = -1;

            player.GetComponent<Player>().resumePlayerControl();
            
            //start the next soundtrack
            SoundManager.Instance.StartSoundTrack(SoundEnum.nocturne);

            transitionInProgress = false;
            gameObject.SetActive(false);

        }
    }
}
