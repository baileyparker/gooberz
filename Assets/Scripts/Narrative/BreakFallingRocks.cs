﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


public class BreakFallingRocks : MonoBehaviour {
    //this should be pulling from the random sprite class, but for now it's in its own class

    public Sprite[] sprites;

    void Awake()
    {
        Assert.IsTrue(sprites.Length > 0, "no sprites to choose from");

        // Choose a random sprite to render
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = sprites[Random.Range(0, sprites.Length - 1)];
    }
}
