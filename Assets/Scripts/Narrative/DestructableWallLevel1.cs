﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableWallLevel1 : DestructableWall {

    public GameObject HideMainOverlay;

    //hide a narrative if any
    public GameObject NarrativeToHide;
    public GameObject NarrativeToActivate;

    protected override void Disappear() {
    	base.Disappear();
        HideMainOverlay.SetActive(false);
        if (NarrativeToHide != null) {
            NarrativeToHide.SetActive(false);
        }
        if (NarrativeToActivate != null) {
            NarrativeToActivate.SetActive(true);
        }
    }
}
