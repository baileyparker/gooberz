﻿using UnityEngine;

public class GenericNarrativeDialog : MonoBehaviour {
    /// <summary>
    /// The dialogue message displayed for the Janitor in this narrative
    /// trigger.
    /// </summary>
    public string message;

    /// <summary>
    /// The duration (in seconds) to display the dialogue message.
    /// </summary>
    public float duration;

    /// <summary>
    /// If true, the narrative is deactivated after a single use
    /// </summary>
    public bool singleUse = false;

    /// <summary>
    /// Chronologically, the next narrative trigger.
    /// </summary>
    public GameObject nextNarrative;

    /// <summary>
    /// Chronologically, the previous narrative trigger.
    /// </summary>
    public GameObject prevNarrative;


    // Leave this narrative item active until the player interacts with the next one
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.name == "Player") {
            if (message.Length != 0) {
                DialogueManager dialogue = GameManager.Instance.dialogueManager;
                dialogue.Display(DialogueSpeaker.Janitor, message, duration);
            }

            if (nextNarrative != null)
            {
                nextNarrative.SetActive(true);
            }

            if (prevNarrative != null)
            {
                prevNarrative.SetActive(false);
            }

            if (singleUse) {
                gameObject.SetActive(false);
            }
        }
    }
}
