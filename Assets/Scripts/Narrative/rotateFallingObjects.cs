﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateFallingObjects : MonoBehaviour {

    private float minSpeed = 7;
    private float maxSpeed = 15;

    public float speed;

    //transform frame of the tiles
    private Vector3 Tx;
    private Vector3 Ty;
    private Vector3 Tz;

    public Vector3 rotation_axis;


	// Use this for initialization
	void Start () {
        speed = 360 / (2 * Mathf.PI) * Random.Range(minSpeed, maxSpeed) * ((Random.value > 0.5) ? 1 : -1);
        Tx = Random.onUnitSphere;
        Ty = Vector3.Cross(Tx, Random.onUnitSphere);
        Tz = Vector3.Cross(Tx, Ty);
        
        rotation_axis = Random.insideUnitSphere;
	}
	
	// Update is called once per frame
	void Update () {
        RotateFrame();
        OrientObject();
        
	}

    void RotateFrame()
    {
        //rotate each vector in the frame
        Tx = Quaternion.AngleAxis(speed * Time.deltaTime, rotation_axis) * Tx;
        Ty = Quaternion.AngleAxis(speed * Time.deltaTime, rotation_axis) * Ty;
        Tz = Quaternion.AngleAxis(speed * Time.deltaTime, rotation_axis) * Tz;

        //ensure they are orthonormal
        Ty = Vector3.Cross(Tz, Tx);
        Tz = Vector3.Cross(Tx, Ty);
    }

    void OrientObject()
    {
        transform.localScale = new Vector3(Tz.x, Tz.y, 1);

        //cheating for now. should use Tx, and Ty to set the proper rotation angle in plane
        transform.Rotate(Vector3.back, speed * Time.deltaTime);
    }
}
