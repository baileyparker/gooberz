﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ActivateLevelZeroToOneTransition : MonoBehaviour {
    //Attach this to the enemy to be destroyed. On destroy, it will activate the transition manager object

    public GameObject transition_manager_obj;

    private void Start()
    {
        transition_manager_obj = Instantiate(transition_manager_obj);//GameObject.Find("LevelTransitionManager");
        transition_manager_obj.transform.SetParent(GameObject.Find("Room 6(Clone)").transform);
        //transition_manager_obj.GetComponent<LevelZeroToOneTransitionNarrative>().hole = GameObject.Find("HoleInTheFloor");
        transition_manager_obj.SetActive(false);
    }

    private void OnDestroy()
    {
        transition_manager_obj.SetActive(true);
        transition_manager_obj.GetComponent<LevelZeroToOneTransitionNarrative>().StartTransition();
    }
}
