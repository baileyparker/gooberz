﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EndgameTransition : MonoBehaviour {

    public GameObject LeftDoor;
    Vector3 LeftDoorInitPos;
    public GameObject RightDoor;
    Vector3 RightDoorInitPos;
    public float maxShakeOffset = 0.1f;
    public float maxShakeAngle = 10f;
    public GameObject wall1; //walls behind elevator need to be disabled
    public GameObject wall2;
    public GameObject battery;

    public GameObject player;

    bool transitionInProgress = false;
    bool firstFrame = true;

    float startTime;
    Vector3 playerStartPos;
    Vector3 camStartPos;
    Vector3 playerPlaceBatteryPos = new Vector3(-9.25f, 7.4f, 0);
    Vector3 batteryPlacePos = new Vector3(-9.25f, 8, 0);
    Vector3 playerFrontOfElevatorPos = new Vector3(-7.5f, 6.25f, 0);
    Vector3 camFrontofElevatorPos = new Vector3(-7.5f, 6.5f, -10f);
    Vector3 playerInsideElevatorPos = new Vector3(-7.5f, 8.5f, 0);
    Vector3 camPanUpPos = new Vector3(-7.5f, 16f, -10f);


    //start at t=0 when the player collides with the narrative trigger
    //for each of these, force the player to be in the same spot (maybe deactivate player control script)
    float key0 = 2.0f;  // walk to place battery (set animation to walking)
    float key1 = 3.0f;  // place battery
    float key2 = 5.0f;  // walk back
    float key3 = 7.0f;  // elevator does stuff (shake)
    float key4 = 9.0f;  // elevator doors open
    float key5 = 11.0f; // player walks in (set elevator door layer to in front of player)
    float key6 = 13.0f;  // doors close
    float key7 = 14.0f;  // camera pans up (i.e. elevator is moving up)


    private void Start()
    {
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        if (transitionInProgress)
        {
            Level1To2Transition();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player" && GameManager.Instance.hud.HasBattery)
        {
            if (!transitionInProgress)
            {
                DialogueManager dialogue = GameManager.Instance.dialogueManager;
                dialogue.Display(DialogueSpeaker.Janitor, "This should be the final elevator out", 7);

                transitionInProgress = true;
                startTime = Time.time;

                playerStartPos = player.transform.position;
                camStartPos = Camera.main.transform.position;
                LeftDoorInitPos = LeftDoor.transform.position;
                RightDoorInitPos = RightDoor.transform.position;

                player.GetComponent<Player>().pausePlayerControl();
            }
        }
    }

    public void Level1To2Transition()
    {
        //need some way to force player position. for now just set it every frame
        float t = Time.time - startTime;

        if (t > 0 && t < key0)
        {
            float f = t / key0; //fraction of this interval for Lerp
            player.transform.position = Vector3.Lerp(playerStartPos, playerPlaceBatteryPos, f);
            player.GetComponent<Animator>().SetFloat("xvel", -0.5f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", 0.1f);
        }

        if (t > key0 && t < key1)
        {
            //initial set up (performed here so that the player doesn't accidentally clip behind the elevator doors)
            if (firstFrame)
            {
                //hide walls behind the elevator doors
                wall1.SetActive(false);
                wall2.SetActive(false);

                //make the elevator doors appear in front of the player (for when they close)
                RightDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;
                LeftDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;

                //place the battery down
                battery.SetActive(true);
                battery.transform.position = batteryPlacePos;

                firstFrame = false;

                // Remove battery from HUD
                GameManager.Instance.hud.GiveBattery(false);
            }

            player.transform.position = playerPlaceBatteryPos;
            player.GetComponent<Animator>().SetFloat("xvel", 0.0f); //turn off player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", 0.0f);
        }

        if (t > key1 && t < key2)
        {
            //initial set up (performed here so that the player doesn't accidentally clip behind the elevator doors)
            if (firstFrame)
            {
                //hide walls behind the elevator doors
                wall1.SetActive(false);
                wall2.SetActive(false);

                //make the elevator doors appear in front of the player (for when they close)
                RightDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;
                LeftDoor.GetComponent<SpriteRenderer>().sortingOrder = 2;

                //place the battery down
                battery.SetActive(true);

                firstFrame = false;
            }

            float f = (t - key1) / (key2 - key1);
            player.transform.position = Vector3.Lerp(playerPlaceBatteryPos, playerFrontOfElevatorPos, f);
            Camera.main.transform.position = Vector3.Lerp(camStartPos, camFrontofElevatorPos, f);
            player.GetComponent<Animator>().SetFloat("xvel", 0.5f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", -0.1f);
        }

        if (t > key2 && t < key3)
        {
            float f = (t - key2) / (key3 - key2);
            float shake = 1 - 2 * Mathf.Pow(f - 0.5f, 1);

            LeftDoor.transform.position = LeftDoorInitPos + maxShakeOffset * shake * ((Vector3)Random.insideUnitCircle + Vector3.back * 10);
            LeftDoor.transform.rotation = Quaternion.Euler(0, 0, maxShakeAngle * shake * ((Random.value * 2) - 1));

            RightDoor.transform.position = RightDoorInitPos + maxShakeOffset * shake * ((Vector3)Random.insideUnitCircle + Vector3.back * 10);
            RightDoor.transform.rotation = Quaternion.Euler(0, 0, maxShakeAngle * shake * ((Random.value * 2) - 1));

            player.transform.position = playerFrontOfElevatorPos;
        }

        if (t > key3 && t < key4)
        {


            float f = (t - key3) / (key4 - key3);
            LeftDoor.transform.position = Vector3.Lerp(LeftDoorInitPos, LeftDoorInitPos + Vector3.left * 0.8f, f);
            RightDoor.transform.position = Vector3.Lerp(RightDoorInitPos, RightDoorInitPos + Vector3.right * 0.8f, f);
            player.transform.position = playerFrontOfElevatorPos;
        }

        if (t > key4 && t < key5)
        {
            float f = (t - key4) / (key5 - key4);
            player.transform.position = Vector3.Lerp(playerFrontOfElevatorPos, playerInsideElevatorPos, f);
        }

        if (t > key5 && t < key6)
        {
            float f = (t - key5) / (key6 - key5);
            LeftDoor.transform.position = Vector3.Lerp(LeftDoorInitPos + Vector3.left * 0.8f, LeftDoorInitPos, f);
            RightDoor.transform.position = Vector3.Lerp(RightDoorInitPos + Vector3.right * 0.8f, RightDoorInitPos, f);
            player.transform.position = playerInsideElevatorPos;
        }

        if (t > key6 && t < key7)
        {
            float f = (t - key6) / (key7 - key6);
            Camera.main.transform.position = Vector3.Lerp(camFrontofElevatorPos, camPanUpPos, f);
            player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0); //set the sprite to invisible
            player.transform.position = Camera.main.transform.position + Vector3.up * 2.25f;
        }

        if (t > key7)
        {
            //GameManager.Instance.levelManager.ImmediatelyLoadRoom(RoomName.Level2Room1, null);
            SceneManager.LoadScene("Endgame");
            //player.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1); //set the sprite to visible
            SoundManager.Instance.StopSoundTrack();
            player.GetComponent<Player>().resumePlayerControl();
            //SoundManager.Instance.StartSoundTrack("nocturne");
        }
    }
}
