﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxWalls : MonoBehaviour {
    //parallax is attached to the camera

    public float speed = 1;

    //if true, then the object's position is updated according to the current camera
    public bool isActive = true;

    float width = 20;//17.8f;
    float height = 12;//10f;

    float camX0 = 10;//8.2f;
    float camY0 = 6;//4.5f;

    //origin of the center of the camera
    Vector3 CamLocalOrigin;
    Vector3 CamWorldOrigin;

    //origin of the wall relative to it's parent object
    Vector3 WallOrigin;

    // Get the initial position of the block in the frame, and set the origin of the camera
    void Start () {
        CamLocalOrigin = new Vector3(camX0, camY0, 0);
        CamWorldOrigin = Camera.main.transform.position;
        WallOrigin = 1 / speed * (new Vector3(transform.localPosition.x, transform.localPosition.y, 0) + CamLocalOrigin);
    }

    // Move the wall relative to the camera
    void Update () {
        if (isActive) {
            transform.localPosition = ModPos(speed, WallOrigin) - CamLocalOrigin;
        }
	}

    //returns the position of the camera modulus the width/height of the camera
    Vector3 ModPos(float speed, Vector3 offset = new Vector3())
    {
        Vector3 pos = (Camera.main.transform.position - CamWorldOrigin + offset) * speed;
        float x = pos.x;
        float y = pos.y;
        return new Vector3((x >= 0) ? x % width : width - (-x) % width, (y >= 0) ? y % height : height - (-y) % height, 0);
    }
}
