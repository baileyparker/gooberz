﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PrologNarrativeController : MonoBehaviour {
    Camera MainCamera;
    public GameObject player;
    int deactivate_player = 0;
    Vector3 initPos;// = new Vector3(6.34f, 2.42f, 0.0f);
    Vector3 finalPos = new Vector3(0.0f, 3.45f, 0.0f);

    ArrayList explosions = new ArrayList();
    public GameObject explosionPrefab;
    public GameObject enemies;
    int enemyCount = 13;

    public GameObject TitleScreen;

    float ortho_init;
    Vector3 camOrigin;
    Vector3 camFinal = new Vector3(0.0f, 7.31f, -10.0f);

    float key0 = 15.0f;  //slow zoom out
    float key1 = 19.0f;  //explode the enemies
    float key2 = 22.0f;  // Player fades in
    float key3 = 27.0f;  // Player walks to entrance
    float key4 = 35.0f;  // Pan up to logos  
    float key5 = 35.82f; // Stop panning, Cut to Title screen (synced with amulet sound effect)
    float key6 = 36.0f;  // Switch to next level

    // etc..

    [Range(0.0f, 1.0f)]
    public float volume = 0.5f;

    // Use this for initialization
    void Start() {

        //SoundManager.Instance.AttachSound(gameObject, SoundEnum.mars);

        MainCamera = Camera.main;
        camOrigin = MainCamera.transform.position;
        initPos = player.transform.position;
    }

    // Update is called once per frame
    void Update() {

        //skip ahead to the title screen
        if (Input.GetButton("Jump")) {
            gameObject.GetComponent<AudioSource>().time = 35.80f;
        }

        if (deactivate_player++ == 0) {
            player.SetActive(false);
        }

        float t = gameObject.GetComponent<AudioSource>().time;

        if (t > 0 && t < key0) {
            // Slow pan out of camera
            MainCamera.orthographicSize = Mathf.Lerp(0.0f, 5.0f, t/key0);
        }

        if (t > key0 && t < key1) {
            // Player blows up the enemies
            if (enemyCount >= 0 && Random.Range(0, 5) == 0) {
                explosions.Add(Instantiate(explosionPrefab, (Vector2)player.transform.position + Random.insideUnitCircle * 2, Quaternion.identity));
                enemies.transform.GetChild(enemyCount--).gameObject.SetActive(false);
            }
        }

        if (t > key1 && t < key2) {
            enemies.SetActive(false);
            player.SetActive(true);
            Color cur = player.GetComponent<SpriteRenderer>().color;
            cur.a = Mathf.Lerp(0.0f, 1.0f, (t - key1) / (key2 - key1));
            player.GetComponent<SpriteRenderer>().color = cur;
        }

        if (t > key2 && t < key3) {
            player.transform.position = Vector3.Lerp(initPos, finalPos, (t - key2) / (key3 - key2));
            player.GetComponent<Animator>().SetFloat("xvel", -0.5f); //set player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", 0.1f);
        }

        if (t > key3 && t < key4) {
            MainCamera.transform.position = Vector3.Lerp(camOrigin, camFinal, (t - key3) / (key4 - key3));
            player.GetComponent<Animator>().SetFloat("xvel", 0.0f); //turn off player walking animation
            player.GetComponent<Animator>().SetFloat("yvel", 0.0f);
        }

        if (t > key4 && t < key5) {
            //stop panning for a short instant
        }
        if (t > key5 && t < key6)
        {
            TitleScreen.SetActive(true);
        }
        if (t > key6) {
            SceneManager.LoadScene("MainScene");
        }
    }
}
