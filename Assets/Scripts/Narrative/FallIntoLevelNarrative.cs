﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FallIntoLevelNarrative : MonoBehaviour {

    private GameObject player;
    public GameObject parallaxWalls;
    public GameObject fallenRocks;
    public GameObject staticFallenRocks;

    private float fall_speed = 10;

    private Vector3 playerStartPosition = new Vector3(-3f, 150, 0f);
    private Vector3 playerEndPosition = new Vector3(-3f, 1f, 0f);
    private Vector3 rocksStartPosition = new Vector3(0, 151, 0);

    private float parallaxEndHeight = 33f;



    private bool firstFrame = true;

    void Start() {
        player = GameObject.Find("Player");
        Assert.IsNotNull(player, "couldn't find player");
        player.GetComponent<Player>().pausePlayerControl();

        //set the starting positions for falling
        player.transform.position = playerStartPosition;

        //turn off collisions so that the player can fall through the walls into the stage
        player.GetComponent<PolygonCollider2D>().enabled = false;

        staticFallenRocks.SetActive(false);
        //Camera.main.transform.position = startPosition + Vector3.back * 10;
    }

    void FixedUpdate() {
        if (firstFrame) {
            //move camera to the starting position
            Camera.main.transform.position = playerStartPosition + Vector3.back * 10;

            //instantiate the parallax walls
            parallaxWalls = Instantiate(parallaxWalls);//.SetActive(true);
            parallaxWalls.transform.SetParent(Camera.main.transform);
            parallaxWalls.transform.localPosition = new Vector3(0, 0, 20);

            //instantiate falling rocks and make them rotate
            fallenRocks = Instantiate(fallenRocks);
            fallenRocks.transform.position = rocksStartPosition;
            fallenRocks.transform.SetParent(Camera.main.transform);
            AttachRotationToRocks();

            //move player to starting position
            player.transform.position = playerStartPosition;
            
            firstFrame = false;
        }
        else {
            float y = player.gameObject.transform.position.y;

            if (y > playerEndPosition.y) {
                //make the player fall regularly
                player.transform.position = new Vector3(playerEndPosition.x, y - fall_speed * Time.deltaTime);

                if (y > parallaxEndHeight)
                {
                    //display parallax
                } else {
                    //disable parallax object
                    //parallaxWalls.SetActive(false);
                    fall_speed = 50;

                    parallaxWalls.transform.SetParent(transform.parent);

                    //Destroy(parallaxWalls);
                }

            } else {
                //last frame of falling
                player.transform.position = playerEndPosition;
                player.GetComponent<PolygonCollider2D>().enabled = true;
                player.GetComponent<Player>().resumePlayerControl();
                Destroy(parallaxWalls);
                Destroy(fallenRocks);
                staticFallenRocks.SetActive(true);
                Destroy(gameObject);

            }
        }
            
    }

    void AttachRotationToRocks() {
        for (int i = 0; i < fallenRocks.transform.childCount; i++) {
            fallenRocks.transform.GetChild(i).gameObject.AddComponent<rotateFallingObjects>();
            //float scale = Random.Range(0.8f, 0.85f);
            //fallenRocks.transform.GetChild(i).transform.localScale = new Vector3(scale, scale, 1);
        }
    }
}
