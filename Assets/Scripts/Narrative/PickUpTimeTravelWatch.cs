﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTimeTravelWatch : GenericNarrativeDialog {

    public GameObject FallenRocks;
    public GameObject HideMainRoom;
    public GameObject DestructableWalls;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.name == "Player") {
            // Hide the watch and the fallen rocks
            gameObject.SetActive(false);
            FallenRocks.SetActive(false);

            // Replace the broken walls and
            HideMainRoom.SetActive(true);

            // Make sure to get all walls that might have been deactivated when shot
            foreach (Transform child in DestructableWalls.transform) {
                child.gameObject.SetActive(true);
            }

            // Activate narrative "weren't there rocks here?"
            if (prevNarrative != null) {
                prevNarrative.SetActive(false);
            }
            if (nextNarrative != null) {
                nextNarrative.SetActive(true);
            }

            GameManager.Instance.hud.ShowWatch();

            //Have the Janitor comment on the situation
            DialogueManager dialogue = GameManager.Instance.dialogueManager;
            dialogue.Display(DialogueSpeaker.Janitor, message, duration);
        }
    }
}
